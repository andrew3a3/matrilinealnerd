<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 500)->nullable();
            $table->string('subtitle')->nullable();
            $table->string('header_image')->nullable();
            $table->bigInteger('user_id');
            $table->foreign('user_id')
            ->references('id')->on('users');
            $table->longText('content')->nullable();
            $table->string('status')->default('in-progress');
            $table->boolean('notify_on_publish')->default(true); //when published email all subscribers about it if true
            /* in-progress - being written
            ready for review - needs to be seen by admin
            authorized - seen by admin, article ready to be published
            needs revision - seen by admin and needs revision
            cancelled - content has been canelled
            retracted - published but now has been pulled*/
            $table->timestamp('publish_on')->nullable();
            $table->timestamp('unpublish_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}