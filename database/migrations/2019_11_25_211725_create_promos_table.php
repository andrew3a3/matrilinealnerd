<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('code')->nullable();

            #discount
            $table->double('dollar', 10, 2)->default(0);
            $table->double('percentage', 10, 2)->default(0);
            $table->boolean('use_dollar')->default(false);

            #choice
            $table->enum('target', ['price', 'product', 'none'])->default('none');

            #products
            $table->bigInteger('target_product');
            $table->foreign('target_product')
                ->references('id')->on('products')->nullable();
            $table->integer('target_qty')->default(1);
            $table->boolean('use_qty')->default(false);

            #price
            $table->double('target_price', 10, 2)->deafult(0);
            $table->timestamps();

            #states
            $table->boolean('active')->default(false);
            $table->timestamp('expires_on')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}