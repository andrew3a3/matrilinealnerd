<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    /*cart items are individual instances of product so that 
    in an extraordinary event(close to selling out, on sale, etc.) if this item is
    still in a users cart they can be emailed about the event*/
    
    protected $fillable = [
        'id','cart', 'user'
    ];
}
