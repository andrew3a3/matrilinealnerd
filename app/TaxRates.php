<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxRates extends Model
{
    protected $fillable = [
        'abbreviation', 'name', 'rate'
    ];

    protected $hidden = ['id', 'created_at'];
}
