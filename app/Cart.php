<?php

namespace App;

class Cart 
{
    public $items; //associative array
    public $totalQty = 0;
    public $totalPrice = 0;
    public $email;

    public function __construct($oldCart) {
        if($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    //increments item in cart
    public function add($item, $id, $quantity = 1) {
        //creating new item
        $storedItem = [
            'qty'=> 0, 
            'price'=> $item->price,
            'item'=> $item
        ];
        
        //if have items in cart
        if($this->items) {
            if(array_key_exists($id, $this->items)) {
                //overriding stored item
                $storedItem = $this->items[$id];
            }
        }

        $storedItem['qty'] += $quantity;

        $addage = $item->price  * $quantity;
        $this->items[$id] = $storedItem;
        $this->totalQty += $quantity;
        $this->totalPrice += $addage;
    }

    //decrements item in cart
    public function reduce($item, $id, $quantity = 1) {
        //if have items in cart
        if($this->items) {
            if(array_key_exists($id, $this->items)) {
                //overriding stored item
                //modify stored item
                $storedItem = $this->items[$id];
                $storedItem['qty'] -= $quantity;

                //update in cart
                $this->items[$id] = $storedItem;
                $this->totalQty -= $quantity;
                $this->totalPrice -= $item->price * $quantity;
            }
        }
    }

    //deletes item from cart
    public function remove($item, $id) {
        //if have items in cart
        if($this->items) {
            if(array_key_exists($id, $this->items)) {
                //overriding stored item
                $storedItem = $this->items[$id];

                //remove element
                unset($this->items[$id]);

                //update cart
                $this->totalQty -= $storedItem['qty'];
                $this->totalPrice -= $storedItem['qty'] * $item->price;
            }
        }
    }
}