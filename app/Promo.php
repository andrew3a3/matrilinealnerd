<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Promo extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'name', 'dollar', 'percentage', 'use_dollar', 'target',
        'target_product', 'target_qty',
        'use_qty', 'target_price', 'expires_on'
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'use_dollar' => 'boolean',
        'use_qty' => 'boolean'
    ];


    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}