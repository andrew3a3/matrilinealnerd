<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Facade\usps;
use App\Order;

class PublicController extends Controller
{
    public function index() {
        return view('welcome');
    }

    public function testUsps() {
        
        //print_r(usps::cityStateLookup('90210'));
        //print_r(usps::trackPackage("90210"));
        //print_r(Usps::getShippingCost(0, .1, "90210"));
        return back();
    }


    public function singlePost($id) {
        return view('singlePost');
    }

    public function about() {
        return view('about');
    }

    public function contact() {
        return view('contact');
    }

    public function contactPost() {
        return view('/');
    }

    public function profile() {
        if(Auth::check() === true) {
            $orders = Auth::user()->orders;
            $orders->transform(function($order,$key){
                $order->cart = unserialize($order->cart);
                return $order;
            });

            //return $orders;
            return view('profile', ['orders'=>$orders]);
        }

        return view('/');
    }


    public function orderSummary($id) {
            $order = Order::where('id',$id)->first();
            if($order != null) {
                $order->cart = unserialize($order->cart);
            }

            //return $orders;
            return view('order-summary', compact('order')); 
    }
}