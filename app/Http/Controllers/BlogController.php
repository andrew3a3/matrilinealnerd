<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\PostView;
use App\Configs\Constants;
use Carbon\Carbon;

class BlogController extends Controller
{
    public function index()
    {
        $maxItems = 9;
        $blogs = Post::where('published', true)->paginate($maxItems);
        return view('blog.index', compact('blogs'));
    }

    public function singlePost($slug)
    {
        $post = Post::where('slug', $slug)
            ->where('published', true)
            ->firstOrFail();

        //create page view item
        $ip = Constants::getIP();
        $view = PostView::where('visitor', $ip)->first();
        if ($view == null) {
            PostView::create([
                'post_id' => $post->id,
                'visitor' => $ip
            ]);
        } else {
            $timeDifference = $view->updated_at->diffInSeconds(Carbon::now());
            if ($timeDifference >= Constants::reviewTime()) {
                $view->count++;
                $view->save();
            }
        }

        return view('blog.singlePost', compact('post'));
    }
}
