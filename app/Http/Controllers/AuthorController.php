<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Post;
use App\User;

//MAIL
use Illuminate\Support\Facades\Notification;
use App\Notifications\PostSubmission;

class AuthorController extends Controller
{
    protected $info;
    public function __construct() {
        $this->middleware('checkRole:author');
        $this->middleware('verified');
    }

    public function dashboard() {
        return view('contributor.dashboard');
    }

    public function deletePost($id) {
        $post = Post::where('id', $id)->first();

        if($post != null) {
            if($post->header_image != null) {
                $image_path = public_path().'/'.$post->header_image;
                unlink($image_path);
            }

            $post->delete();
        }

        return back();
    }

    public function submittedPosts() {
        $posts = Post::where('status', 'ready for review')->get();
        return view('editor.posts', compact('posts'));
    }

    public function posts() {

        $posts = Post::where('user_id',Auth::user()->id)->get();
        return view('contributor.posts', compact('posts'));
    }

    public function createPost() {
        $post = Post::create(['user_id'=>Auth::user()->id]);
        return redirect()->route('cont.SinglePost', $post->id);
    }

    public function getPost($id) {
        $post = Post::findOrFail($id);
        return view('contributor.postEditor', compact('post'));
    }

    public function previewPost($id) {
        $post = Post::findOrFail($id);
        return view('contributor.postPreview', compact('post'));
    }


    public function updatePost(Request $request, $id) {
        $post = Post::find($id);
        
        if($request->title != null) {
            $post->title = $request->title;
        }

        if($post->title != null) {
            $post->slug = Str::slug($post->title, '-');
        }

        if($request->subtitle != null)
            $post->subtitle = $request->subtitle;

        if($request->editor1 != null)
            $post->content = $request->editor1;

        if($request->has('publish_on')){
            $post->publish_on = $request->publish_on;
        }

        if($request->has('unpublish_on')){
            $post->unpublish_on = $request->unpublish_on;
        }

        if($request->hasFile('image')) {
                if($post->header_image != null) {
                    $image_path = public_path().'/'.$post->header_image;
                    unlink($image_path);
                }
                $image = $request->file('image');
                $fileName = $image->getClientOriginalName(); 
                //$fileExtension = $thumbnail->getClientOriginalExtension();
                $image->move('post-images', $fileName);
                $post->header_image = 'post-images/'.$fileName;
        }

        
        if($request->has('post_status')) {
             $post->status = $request->post_status; 
             if($request->post_status != 'authorized') {
                $post->published = false;
             }
        }

        switch ($request->input('action')) {
            case 'submit':
                $post->status = 'ready for review';
                $post->save();
                $this->notifyeditors($post);
                return redirect()->route('cont.Posts');

            default:
                $post->save();
                return back();
        }
    }

    public function comments() {
        return view('contributor.comments');
    }

    public function notifyeditors($post) {
        $editors = User::where('editor', true)->where('notifications', true)->get();
        foreach($editors as $editor) {
            Notification::route('mail', $editor->email)->notify
                (new PostSubmission($post));
        }
    }
}