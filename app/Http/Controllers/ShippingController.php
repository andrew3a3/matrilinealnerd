<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Facade\usps;
use App\TaxRates;

class ShippingController extends Controller
{
    public function shippingCost(Request $request) {
        if($request->ajax()) {
            return usps::getShippingCost(0, .1, $request->zipcode);
        }

        return back();
    }

    public function lookup(Request $request) {
        if($request->ajax()) {
            return usps::cityStateLookup($request->zipcode);
        }

        return back();
    }

    public function tracking(Request $request) {
        if($request->ajax()) {
            return usps::trackPackage($request->tracking);
        }

        return back();
    }

    public static function confirm($tracking) {
        $desired = "delivered";

        $summary = usps::trackPackage($tracking);
        if(strpos($summary, $desired) !== false) {
            return True;
        }

        return False;      
        //return usps::confirmDelivery($request->tracking);
    }

    public function taxRate(Request $request) {
        if($request->ajax()) {
            $tax =  TaxRates::where('abbreviation', strtoupper($request->state))->first();
            if($tax) {
                return $tax->rate;
            }

            return null;
        }

        return back();
    }
}