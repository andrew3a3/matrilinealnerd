<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\Cart;
use App\CartItem;
use Session;

class ProductController extends Controller
{
    public function addToCart(Request $request, $id) {
        $product = Product::findOrFail($id);
        $oldCart = Session::has('cart')? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);

        if(Auth::check() == true) {
            $user = Auth::user()->id;
            //save to database to alert people when stock is low or on sale
            $stored = CartItem::where('user', $user)
            ->first();

            $this->updateStoredCart($cart);
        }

        return back();
    }

    private function updateStoredCart($cart) {
        $user = Auth::user()->id;
            //save to database to alert people when stock is low or on sale
            $stored = CartItem::where('user', $user)
            ->first();

            if($stored) {
                $stored->cart = \serialize($cart);
                $stored->save();
            }else{
                CartItem::create([
                    'user'=>$user,
                    'cart'=>\serialize($cart)
            ]);
        }
    }

    public function reduceItemInCart(Request $request, $id) {
        $product = Product::findOrFail($id);
        $oldCart = Session::has('cart')? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        //decrease if greater than zero
        if($cart->items[$id]['qty'] > 0) {
            $cart->reduce($product, $product->id);
            $request->session()->put('cart', $cart);

            if(Auth::check() == true) {
                $this->updateStoredCart($cart);
            }
        }

        return back();
    }

    public function removeFromCart(Request $request, $id) {
        $product = Product::findOrFail($id);
        $oldCart = Session::has('cart')? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->remove($product, $product->id);

        if($cart->totalQty <= 0) {
            $request->session()->forget('cart');
            if(Auth::check() == true) {
                CartItem::where('user', Auth::user()->id)
                ->first()
                ->delete();
            }
        }else{
            $request->session()->put('cart', $cart);

            if(Auth::check() == true) {
                $this->updateStoredCart($cart);
            }
        }

        return back();
    }

    //returns if cart is present in database or in sessions
    public static function hasCart() {
        if(Session::has('cart')) {
            return true;
        }

        if(Auth::check() == true) {
            $stored = CartItem::where('user', Auth::user()->id)->first();

            if($stored != null) {
                $stored = unserialize($stored->cart);
                //restore cart on session
                Session::put('cart', $stored);
            }

            return $stored != null;
        }else{
            return false;
        }
    }

    public function getCart() {
        if(!Session::has('cart')) {
            //see if you can pull a cart from the database
            if(Auth::check() == true) {
                $stored = CartItem::where('user', Auth::user()->id)->first();

                if($stored != null) {
                    $stored = unserialize($stored->cart);
                    //restore cart on session
                    Session::put('cart', $stored);
            
                    $cart = $stored->cart;
                    return view('shop.shopping-cart', 
                        ['products'=>$cart], ['totalPrice'=>$cart->totalPrice]);
                }    
            }

            return view('shop.shopping-cart', ['products'=>null]);
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        //update cart in database
        if(Auth::check() == true) {
            $this->updateStoredCart($cart);
        }

        return view('shop.shopping-cart', ['products'=>$cart->items], ['totalPrice'=>$cart->totalPrice]);
    }
}
