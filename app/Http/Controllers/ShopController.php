<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use App\Order;
use App\Cart;
use App\CartItem;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;
use App\Facade\Square;
use App\Facade\Paypal;
use PayPal\Api\Payment;
use App\Configs\Constants;
use Session;
use SquareConnect\Model\CreatePaymentResponse;
use App\ProductView;
use Carbon\Carbon;

//MAIL
use Illuminate\Support\Facades\Notification;
use App\Notifications\PurchaseConfirmation;
use App\Notifications\PurchaseNotification;

class ShopController extends Controller
{

    public function index()
    {
        $maxItems = 12;
        //Multi-categorization
        $arr = array();
        $categories = Constants::categories();
        foreach ($categories as $category) {
            $curr = Product::where('category', $category)->paginate($maxItems);
            array_push($arr, $curr);
        }


        $views = collect($arr);

        return view('shop.index', compact('views', 'categories'));
    }

    public function getCheckout()
    {
        if (!ProductController::hasCart()) {
            //if cart empty return to shop
            return view('shop.shopping-cart', ['products' => null]);
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;
        return view('shop.checkout', ['total' => $total, 'items' => $cart->items]);
    }

    public function processPayment(Request $request)
    {
        if (!ProductController::hasCart())
            return back();

        //extract data
        $name = $request->name;
        $line1 = $request->address;
        $email = $request->email;
        $city = $request->city;
        $state = $request->state;
        $zipcode = $request->zipcode;
        $shippingRate = $request['shipping-rate'];
        $taxRate = $request['tax-rate'];
        $subtotal = $request['total-price'];

        $address = str_replace(";", "", $line1) . ";" .
            str_replace(";", "", $city) . ";" .
            str_replace(";", "", $state) . ";" .
            str_replace(";", "", $zipcode);

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        $amount = $subtotal + $taxRate  + $shippingRate;

        //charge card
        $square = new Square;
        //capturing payment id
        $result = $square->execute($request['nonce'], intval(($amount) * 100));

        $id = Order::getOrderId();
        while (Order::find($id) != null) {
            $id = Order::getOrderId();
        }

        $order = Order::create([
            'id' => $id,
            'cart' => serialize($cart),
            'user_id' => Auth::check() == true ? Auth::user()->id : -1,
            'address' => $address,
            'name' => $name,
            'payment_id' => $result,
            'email' => $email,
            'payment_method' => 'square',
            'pre_price' => $cart->totalPrice,
            'post_price' => $subtotal,
            'shipping' => $shippingRate,
            'tax' => $taxRate
        ]);

        Session::forget('cart');

        //clear database cart
        if (Auth::check() == true) {
            $item = CartItem::where('user', Auth::user()->id)
                ->first();

            if ($item != null)
                $item->delete();
        }

        $total = $order->post_price + $order->shipping + $order->tax;
        Notification::route('mail', $email)->notify(new
            PurchaseConfirmation(
                $order->id,
                $total,
                $order->address,
                $order->name,
                'card'
            ));

        $this->notifyAdmin($order->id, $total);
        return redirect()->route('shop.orderConfirmation');
    }

    public function singleProduct($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();

        //create page view item
        $ip = Constants::getIP();
        $view = ProductView::where('visitor', $ip)->first();
        if ($view == null) {
            ProductView::create([
                'product_id' => $product->id,
                'visitor' => $ip
            ]);
        } else {
            $timeDifference = $view->updated_at->diffInSeconds(Carbon::now());
            if ($timeDifference >= Constants::reviewTime()) {
                $view->count++;
                $view->save();
            }
        }

        return view('shop.singleProduct', compact('product'));
    }

    //show paypal checkout
    public function getPaypal()
    {
        if (!ProductController::hasCart()) {
            //if cart empty return to shop
            return view('shop.shopping-cart', ['products' => null]);
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;
        return view('shop.paypal', ['total' => $total, 'items' => $cart->items]);
    }

    //execute first step of paypal payment
    public function paypal(Request $request)
    {
        if (!ProductController::hasCart()) {
            return back();
        }

        //extract data
        $name = $request->name;
        $email = $request->email;
        $line1 = $request->address;
        $city = $request->city;
        $state = $request->state;
        $zipcode = $request->zipcode;
        $shippingRate = $request['shipping-rate'];
        $taxRate = $request['tax-rate'];
        $subtotal = $request['total-price'];

        Session::put('email', $email);

        $paypal = new Paypal;

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        $discount = $subtotal -  $cart->totalPrice;
        foreach ($cart->items as $item) {
            $paypal->addItem(
                $item['item']['title'],
                $item['price'],
                $item['qty']
            );
        }

        $paypal->addItem('discount', $discount, 1);


        $paypal->setDetails(
            floatval($shippingRate),
            floatval($subtotal),
            floatval($taxRate)
        );
        $paypal->execute($name, $line1, $city, $state, $zipcode, 'US');
    }

    //execute final step of paypal payment
    public function processPaypal(Request $request)
    {

        $paypal = new Paypal;
        $payment = $paypal->executePayment();

        $cart = Session::get('cart');

        $id = Order::getOrderId();
        while (Order::find($id) != null) {
            $id = Order::getOrderId();
        }

        $data = json_decode($payment);
        // return $data->payer->payer_info->email;

        $email =  Session::get('email');
        Session::forget('email');
        $order = Order::create([
            'id' => $id,
            'cart' => serialize($cart),
            'user_id' => Auth::check() == true ? Auth::user()->id : -1,
            'address' => str_replace(";", "", $data->payer->payer_info->shipping_address->line1) . ";" .
                str_replace(";", "", $data->payer->payer_info->shipping_address->city) . ";" .
                str_replace(";", "", $data->payer->payer_info->shipping_address->state) . ";" .
                str_replace(";", "", $data->payer->payer_info->shipping_address->postal_code),
            'name' => $data->payer->payer_info->shipping_address->recipient_name,
            'email' => $email,
            'payment_id' => $data->id,
            'payment_method' => 'paypal',
            'shipping' => $data->transactions[0]->amount->details->shipping,
            'pre_price' => $cart->totalPrice,
            'post_price' => $data->transactions[0]->amount->details->subtotal,
            'tax' => $data->transactions[0]->amount->details->tax
        ]);

        Session::forget('cart');

        //clear database cart
        if (Auth::check() == true) {
            $item = CartItem::where('user', Auth::user()->id)
                ->first();

            if ($item != null)
                $item->delete();
        }

        $total = $order->post_price + $order->shipping + $order->tax;
        Notification::route('mail', $email)->notify(new
            PurchaseConfirmation(
                $order->id,
                $total,
                $order->address,
                $order->name,
                'paypal'
            ));

        $this->notifyAdmin($order->id, $total);
        return redirect()->route('shop.orderConfirmation');
    }

    //go to confirmation page
    public function confirmation()
    {
        return view('confirmation');
    }


    public function notifyAdmin($order, $price)
    {
        $admins = User::where('seller', true)->where('notifications', true)->get();
        foreach ($admins as $admin) {
            Notification::route('mail', $admin->email)->notify(new PurchaseNotification($order, number_format($price, 2)));
        }
    }
}
