<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Product;
use App\Order;
use App\Configs\Constants;

//MAIL
use Illuminate\Support\Facades\Notification;
use App\Notifications\OrderUpdate;

class AdminController extends Controller
{
    private static $info;

    public function __construct()
    {
        $this->middleware('checkRole:admin');
        $this->middleware('verified');
    }

    public function dashboard()
    {
        $info = self::$info;
        //return $info;
        return view('dashboard');
    }

    #SHOP
    public function awaitingShipment()
    {
        $info = self::$info;
        $orders = Order::where('status', 'pending')->get();
        $orders->transform(function ($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });
        return view('admin.awaitingShipment', compact('orders', 'info'));
    }


    #CONTENT-------------------------------------------
    public function posts()
    {
        return view('admin.posts');
    }

    public function comments()
    {
        return view('admin.comments');
    }

    public function users()
    {
        return view('admin.users');
    }

    #SHOPPING---------------------------------------------------
    public function products()
    {
        $products = Product::all();
        return view('admin.products', compact('products'));
    }

    //hide from view instead of deleting
    public function deleteProduct($id)
    {
        Product::findOrFail($id)->delete();
        return back();
    }

    public function newProduct()
    {
        return view('admin.newProduct');
    }

    public function newProductPost(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string'
        ]);

        $product = new Product;
        $product->title = $request->title;
        $product->slug = Str::slug($request->title, '-');
        $product->category = $request->category;
        $product->pounds = $request->pounds;
        $product->ounces = $request->ounces;

        if ($request->has('editor1')) {
            $product->description = $request->editor1;
        }

        if ($request->hasFile('thumbnail')) {
            if ($product->image != null) {
                $image_path = public_path() . '/' . $product->image;
                unlink($image_path);
            }

            $thumbnail = $request->file('thumbnail');
            $fileName = $thumbnail->getClientOriginalName();
            //$fileExtension = $thumbnail->getClientOriginalExtension();
            $thumbnail->move('product-images', $fileName);
            $product->image = 'product-images/' . $fileName;
        }

        if ($request->has('price')) {
            $product->price = $request->price;
        }


        $product->save();
        return redirect()->route('adminEditProduct', ['id' => $product->id]);
    }

    public function editProduct($id)
    {
        $product = Product::findOrFail($id);
        $categories = Constants::categories();
        //
        return view('admin.editProduct', compact('product', 'categories'));
    }

    public function editProductPost(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $this->validate($request, [
            'title' => 'required|string'
        ]);

        $product->title = $request->title;
        $product->slug = Str::slug($request->title, '-');
        $product->category = $request->category;
        $product->pounds = $request->pounds;
        $product->ounces = $request->ounces;

        if ($request->has('editor1')) {
            $product->description = $request->editor1;
        }

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $fileName = $thumbnail->getClientOriginalName();
            //$fileExtension = $thumbnail->getClientOriginalExtension();
            $thumbnail->move('product-images', $fileName);
            $product->image = 'product-images/' . $fileName;
        }

        if ($request->has('price')) {
            $product->price = $request->price;
        }

        $product->save();

        return back();
    }

    public function editOrder($id, Request $request)
    {
        $order = Order::where('id', $id)->first();

        if ($order == null)
            return 'Order Error: No such order can be found';

        //verify an actual status change was made, if so send a email
        $change = $order->status != $request->status;

        $order->status = $request->status;

        if ($request->has('tracking_number')) {
            $order->tracking_number = $request->tracking_number;
        }

        $order->save();
        if ($order->status != 'pending' && $change == true) {
            Notification::route('mail', $order->email)->notify(new OrderUpdate($order));
        }
        return back();
    }
}
