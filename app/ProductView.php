<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductView extends Model
{
    protected $fillable = [
        'id', 'product_id', 'visitor', 'count'
    ];

    public function product() {
        return $this->belongsTo('App\Product');
    }
}
