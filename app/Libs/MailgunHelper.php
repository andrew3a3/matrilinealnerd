<?php
namespace App\Libs;
require 'vendor/autoload.php';
use Mailgun\Mailgun;

class MailgunHelper {

    public function __construct() {}
        
    public static function sendEmail($name, $to, $subject, $text) {
        # Instantiate the client.
        $mgClient = new Mailgun($_ENV["MAILGUN_API_KEY"]);
        $domain = "MatrilinealNerd";
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'	=> 'Matrilineal Nerd <'.$_ENV["MAILGUN_ADDRESS"].'>',
            'to'	=> $name.' <'.$to.'>',
            'subject' => $subject,
            'text'	=> $text
        ));
    }
}

?>