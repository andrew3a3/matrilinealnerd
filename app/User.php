<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'admin', 'author', 'notifications', 
            'seller', 'editor'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'admin'=>'boolean',
        'author'=>'boolean',
        'seller'=>'boolean',
        'editor'=>'boolean',
        'notifications'=>'boolean'
    ];

    public function setAdminAttribute($value) {
        $this->attributes['admin'] = 
            (int)filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }


    public function setAuthorAttribute($value) {
        $this->attributes['author'] = 
            (int)filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    
    public function setSellerAttribute($value) {
        $this->attributes['seller'] = 
            (int)filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    
    public function setEditorAttribute($value) {
        $this->attributes['editor'] = 
            (int)filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function setNotificationsAttribute($value) {
        $this->attributes['notifcations'] = 
            (int)filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function orders() {
        return $this->hasMany('App\Order');
    }

    public function posts() {
        return $this->hasMany('App\Post');
    }
}