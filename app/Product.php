<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'title', 'description', 'price', 'image', 'slug',
        'pounds', 'ounces'
    ];

    protected $dates = ['deleted_at'];


    public function views()
    {
        return $this->hasMany('App\ProductView');
    }

    public function promos()
    {
        return $this->hasMany('App\Promo');
    }
}