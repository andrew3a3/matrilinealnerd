<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Configs\Constants;

class PostView extends Model
{
    protected $fillable = [
        'id', 'post_id', 'visitor', 'count'
    ];


    /*public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->ip = Constants::getIP();
        });
    }*/

    public function post() {
        return $this->belongsTo('App\Post');
    }
}
