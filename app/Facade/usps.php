<?php
namespace App\Facade;

class usps {
public static function confirmDelivery($tracking) {
    $ip = $_ENV["IP_ADDRESS"];
    $request_doc_template = <<<EOT
    <?xml version="1.0" encoding="UTF-8" ?>
    <TrackRequest USERID="{$_ENV["USPS_USER"]}">
        <Revision>1</Revision>
        <TrackID ID="{$tracking}"></TrackID>
        <ClientIp>{$ip}</ClientIp>
        <SourceID>Matrilineal Nerd</SourceID>
    </TrackRequest>
EOT;

return self::getData($request_doc_template,
    "https://secure.shippingapis.com/shippingapi.dll?API=TrackV2&XML=")->TrackInfo->Status;
}

public static function trackPackage($trackingId) {
    $request_doc_template = <<<EOT
    <?xml version="1.0" encoding="UTF-8" ?>
    <TrackRequest USERID="{$_ENV["USPS_USER"]}">
        <TrackID ID="{$trackingId}"></TrackID>
    </TrackRequest>
EOT;

//returns as a std class object
return self::getData($request_doc_template, 
    "https://secure.shippingapis.com/shippingapi.dll?API=TrackV2&XML=")->TrackInfo->TrackSummary;
}


public static function getShippingCost($pounds, $ounces, $zipDestination) {
        $request_doc_template = <<<EOT
        <?xml version="1.0" encoding="UTF-8" ?>
        <RateV4Request USERID="{$_ENV["USPS_USER"]}">
            <Package ID="0">
                <Service>PRIORITY</Service>
                <ZipOrigination>{$_ENV["USPS_ORIGIN_ZIP"]}</ZipOrigination>
                <ZipDestination>{$zipDestination}</ZipDestination>
                <Pounds>{$pounds}</Pounds>
                <Ounces>{$ounces}</Ounces>
                <Container>VARIABLE</Container>
                <Size>REGULAR</Size>
            </Package>
        </RateV4Request>
EOT;

    //returns as a std class object
    return self::getData($request_doc_template,
        "https://secure.shippingapis.com/shippingapi.dll?API=RateV4&XML=")->Package->Postage->Rate;
    }

public static function cityStateLookup($zipcode) {
    $request_doc_template = <<<EOT
    <?xml version="1.0" encoding="UTF-8" ?>
        <CityStateLookupRequest USERID="{$_ENV["USPS_USER"]}">
            <ZipCode>
                <Zip5>{$zipcode}</Zip5>
            </ZipCode>
        </CityStateLookupRequest>
EOT;

        $result =  self::getData($request_doc_template,
        "https://secure.shippingapis.com/ShippingAPI.dll?API=CityStateLookup&XML=");
        //return $result;
        return json_encode (array(
            'city'=>$result->ZipCode->City,
            'state'=>$result->ZipCode->State
            ));
        }

        public static function VerifyAddress($address, $city, $state, $zip) {
        $request_doc_template = <<<EOT
        <?xml version="1.0" encoding="UTF-8" ?>
            <AddressValidateRequest USERID="{$_ENV["USPS_USER"]}">
                <Address>
                    <Address1></Address1>
                    <Address2>{$address}</Address2>
                    <City>{$city}</City>
                    <State>{$state}</State>
                    <Zip5>{$zip}</Zip5>
                    <Zip4></Zip4>
                </Address>
            </AddressValidateRequest>
EOT;

            return self::getData($request_doc_template,
            "https://secure.shippingapis.com/ShippingAPI.dll?API=Verify&XML=");
            }

            private static function getData($request_doc_template, $preface) {
            //prepare xml doc for query string
            $doc_string = preg_replace('/[\t\n]/', '', $request_doc_template);
            $doc_string = urlencode($doc_string);

            $url = $preface.$doc_string;
            //echo $url."\n\n";

            //perform the get
            $response = file_get_contents($url);
            $xml = simplexml_load_string($response) or die("Error: Cannot create object");

            //returns as a std class object
            return $xml;
            }
        }
?>