<?php
namespace App\Facade;

class Square {
    private $api_client;

     public function __construct() {
         # The access token to use in all Connect API requests. Use your *sandbox* access
        # token if you're just testing things out.
        $access_token = ($_ENV["USE_PROD"] == 'true')  ?  $_ENV["SQUARE_PROD_ACCESS_TOKEN"]
        :  $_ENV["SQUARE_SANDBOX_ACCESS_TOKEN"];
        # Set 'Host' url to switch between sandbox env and production env
        # sandbox: https://connect.squareupsandbox.com
        # production: https://connect.squareup.com
        $host_url = ($_ENV["USE_PROD"] == 'true')  ?  "https://connect.squareup.com"
        :  "https://connect.squareupsandbox.com";

        $api_config = new \SquareConnect\Configuration();
        $api_config->setHost($host_url);
        # Initialize the authorization for Square
        $api_config->setAccessToken($access_token);
        $this->api_client = new \SquareConnect\ApiClient($api_config);

    }

    public function execute($nonce, $amount, $currency = "USD") {        
        # create an instance of the Payments API class
        $payments_api = new \SquareConnect\Api\PaymentsApi($this->api_client);

        $request_body = array (
            "source_id" => $nonce,
            "amount_money" => array (
              "amount" => $amount,
              "currency" => $currency
            ),
            "idempotency_key" => uniqid()
          );
          
          try {
            $result = $payments_api->createPayment($request_body);
            //print_r($result);
            return $result->getPayment()->getId();
          } 
          catch (\SquareConnect\ApiException $e) {
            //echo "Exception when calling PaymentsApi->createPayment:";
            ///var_dump($e->getResponseBody());
            return null;
          }
    }
}
?>