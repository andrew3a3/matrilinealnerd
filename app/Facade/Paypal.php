<?php
namespace App\Facade;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\ShippingAddress;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class Paypal {
    private $apiContext;
    private $payer;
    private $arr;
    private $details;
    private $amount;

    public function __construct() {

        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                ($_ENV["USE_PROD"] == 'true'? $_ENV["PAYPAL_PRODUCTION_CLIENT_ID"]:
                    $_ENV["PAYPAL_SANDBOX_CLIENT_ID"]),
                ($_ENV['USE_PROD'] == 'true'? $_ENV["PAYPAL_PRODUCTION_SECRET"]: 
                    $_ENV["PAYPAL_SANDBOX_SECRET"])
            )
        );

        $this->apiContext->setConfig(
            array(
                'mode' => ($_ENV['USE_PROD'] == 'true'?'live':'sandbox'),
                'log.LogEnabled' => true,
                'log.FileName' => '../PayPal.log',
                'log.LogLevel' => ($_ENV['USE_PROD'] == 'true'? 'INFO': 'DEBUG'), // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                'cache.enabled' => true,
            )
        );

        $this->payer = new Payer();
        $this->payer->setPaymentMethod("paypal");
        $this->arr = array();
    }

    public function addItem($name, $price, $quantity, $currency='USD') {
        $item = new Item();
        $item->setName($name)
        ->setCurrency($currency)
        ->setQuantity($quantity)
        ->setPrice($price);

        array_push($this->arr, $item);
    }

    public function setDetails($shipping, $subtotal, $tax) {
        $this->details = new Details();
        $this->details->setShipping($shipping)
            ->setTax($tax)
            ->setSubtotal($subtotal);

        $total = $subtotal + $tax + $shipping;
        $this->amount = new Amount();
            $this->amount->setCurrency("USD")
                ->setTotal($total)
                ->setDetails($this->details);
    }

    public function execute($name, $line1, $city, $state, $zipcode, $countryCode) {
        $itemList = new ItemList();
        $itemList->setItems($this->arr);

        $shipping_address = new ShippingAddress();
        $shipping_address->setCity($city);
        $shipping_address->setCountryCode($countryCode);
        $shipping_address->setPostalCode($zipcode);
        $shipping_address->setLine1($line1);
        $shipping_address->setState($state);
        $shipping_address->setRecipientName($name);

        $itemList->setShippingAddress($shipping_address);
        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. 
        $transaction = new Transaction();
        $transaction->setAmount($this->amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after 
        // payment approval/ cancellation.
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(route('shop.processPaypal'))
            ->setCancelUrl(route('shop.getPaypal'));
        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent set to 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($this->payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));
            
        // For Sample Purposes Only.
        $request = clone $payment;
        // ### Create Payment
        // Create a payment by calling the 'create' method
        // passing it a valid apiContext.
        // (See bootstrap.php for more on `ApiContext`)
        // The return object contains the state and the
        // url to which the buyer must be redirected to
        // for payment approval
        try {
            $payment->create($this->apiContext);
        } catch (\Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            print("Created Payment Using PayPal. Please visit the URL to Approve.".$request);
            exit(1);
        }
        
        
        /*catch (PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData(); // Prints the detailed error message 
            die($ex);
        }*/

        // ### Get redirect url
        // The API response provides the url that you must redirect
        // the buyer to. Retrieve the url from the $payment->getApprovalLink()
        // method
        $approvalUrl = $payment->getApprovalLink();
        // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
        //print("Created Payment Using PayPal. Please visit the URL to Approve." . "<a href='".$approvalUrl."' >".$approvalUrl."</a>");
        header( "Location: ".$approvalUrl);
        return $payment;
    }

    public function charge($id) {
                // ### Payer
        // A resource representing a Payer that funds a payment
        // For paypal account payments, set payment method
        // to 'paypal'.
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
        // ### Itemized information
        // (Optional) Lets you specify item wise
        // information
        $item1 = new Item();
        $item1->setName('Ground Coffee 40 oz')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku("123123") // Similar to `item_number` in Classic API
            ->setPrice(7.5);
        $item2 = new Item();
        $item2->setName('Granola bars')
            ->setCurrency('USD')
            ->setQuantity(5)
            ->setSku("321321") // Similar to `item_number` in Classic API
            ->setPrice(2);
        $itemList = new ItemList();
        $itemList->setItems(array($item1, $item2));
        // ### Additional payment details
        // Use this optional field to set additional
        // payment information such as tax, shipping
        // charges etc.
        $details = new Details();
        $details->setShipping(1.2)
            ->setTax(1.3)
            ->setSubtotal(17.50);
        // ### Amount
        // Lets you specify a payment amount.
        // You can also specify additional details
        // such as shipping, tax.
        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal(20)
            ->setDetails($details);
        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. 
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());
        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after 
        // payment approval/ cancellation.
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(route('shop.executeOrder', $id))
            ->setCancelUrl(route('shop.index'));
        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent set to 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));
        // For Sample Purposes Only.
        $request = clone $payment;
        // ### Create Payment
        // Create a payment by calling the 'create' method
        // passing it a valid apiContext.
        // (See bootstrap.php for more on `ApiContext`)
        // The return object contains the state and the
        // url to which the buyer must be redirected to
        // for payment approval
        try {
            $payment->create($this->apiContext);
        } catch (\Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            print("Created Payment Using PayPal. Please visit the URL to Approve.".$request);
            exit(1);
        }
        // ### Get redirect url
        // The API response provides the url that you must redirect
        // the buyer to. Retrieve the url from the $payment->getApprovalLink()
        // method
        $approvalUrl = $payment->getApprovalLink();
        header( "Location: ".$approvalUrl);
        return $payment;
    }

    public function executePayment() {
            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $this->apiContext);

            $execution = new PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);

            /*$transaction = new Transaction();
            $amount = new Amount();
            $details = new Details();
            $details->setShipping(2.2)
                ->setTax(1.3)
                ->setSubtotal(17.50);
            $amount->setCurrency('USD');
            $amount->setTotal(21);
            $amount->setDetails($details);
            $transaction->setAmount($amount);

            $execution->addTransaction($transaction);*/
            try {
                // Execute the payment
                // (See bootstrap.php for more on `ApiContext`)
                $result = $payment->execute($execution, $this->apiContext);
                // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                print("Executed Payment" . "Payment" . $payment->getId());
                try {
                    $payment = Payment::get($paymentId, $this->apiContext);
                } catch (Exception $ex) {
                    // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                    print("Get Payment". "Payment");
                    exit(1);
                }
            } catch (Exception $ex) {
                // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                print("Executed Payment". "Payment");
                exit(1);
            }
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            //print("Get Payment". "Payment". $payment->getId().$payment);
            return $payment;
        }    
    }
?>