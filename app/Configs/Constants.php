<?php
namespace App\Configs;
use App\Order;
use App\Post;

class Constants {

   private static $categories = ['Earrings', 'Skin Care', 'Home Decor'];
   private static $min_review_time = 60; //time difference required to count as a valid item review time

   private static $posts = [
         'in-progress',
         'ready for review',
         'authorized',
         'needs revision',
         'cancelled',
         'retracted'
      ];

   public static function categories() {
      //
      return self::$categories;
   }

   public static function reviewTime() {
      return self::$min_review_time;
   }

   public static function postStates() {
      return self::$posts;
   }

   public static function adminUpdates() {
      $info = [
         'awaiting_shipment' => Order::where('status', 'pending')->count(),
         'awaiting_approval' => Post::where('status', 'ready for review')->count()
      ];

      return $info;
   }

   public static function getIP() {
      if(!empty($_SERVER['HTTP_CLIENT_IP'])){
         //ip from share internet
         $ip = $_SERVER['HTTP_CLIENT_IP'];
      }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
         //ip pass from proxy
         $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      }else{
         $ip = $_SERVER['REMOTE_ADDR'];
      }
      
     return $ip;
   }
}
?>