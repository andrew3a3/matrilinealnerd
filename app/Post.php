<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    protected $fillable = [
        'title', 'subtitle', 'header_image',
        'user_id', 'content', 'status',
        'notify_on_publish', 'publish_on',
        'unpublish_on', 'notification_sent', 'slug', 'published'
    ];

    protected $casts = [
        'notify_on_publish' => 'boolean',
        'notification_sent' => 'boolean',
        'published' => 'boolean'
    ];


    public function setPublishOnAttribute($value) {
        if($value == null)
            return;

        $value = Carbon::parse($value);
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $value, 
            'America/Los_Angeles');
        $date->setTimezone('UTC');
        $this->attributes['publish_on'] = $date;
    }
    
    public function setUnpublishOnAttribute($value) {
        if($value == null)
            return;

        $value = Carbon::parse($value);
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $value, 
            'America/Los_Angeles');
        $date->setTimezone('UTC');
        $this->attributes['unpublish_on'] = $date;
    }


    public function getPublishOnAttribute($value) {
        if($value == null)
            return $value;

        $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);
        return $date;
    }

    public function getUnpublishOnAttribute($value) {
        if($value == null)
            return $value;

        $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);
        return $date;
    }

    public function localPublishTime() {
        if($this->publish_on == null)
            return null;

        $date = Carbon::createFromFormat('Y-m-d H:i:s', $this->publish_on);
        $date->setTimezone('America/Los_Angeles');
        return $date;
    }

    public function localUnpublishTime() {
        if($this->unpublish_on == null)
            return null;

        $date = Carbon::createFromFormat('Y-m-d H:i:s', $this->unpublish_on);
        $date->setTimezone('America/Los_Angeles');
        return $date;
    }

    public function styledLocalPublishTime() {
        if($this->publish_on == null)
            return null;

        $date = Carbon::createFromFormat('Y-m-d H:i:s', $this->publish_on);
        $date->setTimezone('America/Los_Angeles');
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('m.d.Y'); 
    }


    public function setNotifyOnPublishAttribute($value) {
        $this->attributes['notify_on_publish'] = 
            (int)filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function setNotificationSentAttribute($value) {
        $this->attribute['notification_sent'] = 
            (int)filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function getCreatedAtAttribute($date) {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('m.d.Y');
    }

    public function getUpdatedAtAttribute($date) {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function views() {
        return $this->hasMany('App\PostView');
    }

}