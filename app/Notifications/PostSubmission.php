<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Post;
use Illuminate\Support\HtmlString;

class PostSubmission extends Notification
{
    use Queueable;
    protected $post;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($post)
    {
        $this->post = $post;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $published = $this->post->publish_on == null? 'undetermined date' : $this->post->publish_on;
        $url = 'https://matrilinealnerd.com/public/contributer/post/'.$this->post->id;

        $message = 'A new article with the title of <i>"'.$this->post->title.'"</i> and a publish date of <b>'.$published.
        '</b> has been submitted for you approval. Click the link below to review submission';

        return (new MailMessage)
            ->subject('An article has been submitted for your approval')
            ->greeting('Greetings')
            ->line(new HtmlString($message))
            ->action('Review Submission', url($url));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
