<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;
use App\Order;

class OrderUpdate extends Notification
{
    use Queueable;
    protected $order;
    

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subjects = [
            'shipped'=>"Your order has shipped",
            'completed'=>"Your order has been delivered",
            'cancelled'=>"Your order has been cancelled"
        ];

        $greetings = [
            'shipped'=>'Heads up',
            'completed'=>'Great news!',
            'cancelled'=>'Hello'
        ];

        $messages = [
            'shipped'=>
                '<center><div>
                                <h2>Your order has shipped!</h2>             
                                <div style="margin-top:25px;">
                                    You can always <i>check the status of your order</i> by either <br>
                                    clicking the link below or logging into your profile at <a href="www.matrilinealnerd.com">
                                        <i>Matrilineal
                                            Nerd</i></a>
                                    to view more details.
                                </div>              
                                <div style="margin-top: 25px;">
                                    <small>
                                        Status update for Order# '.$this->order->id.'
                                    </small>
                                    <br><br>
                                    <b><i>Your tracking number is '.$this->order->tracking_number.'</i></b>
                                </div>
                </div></center>',
            'completed'=>'  
                    <center>
                        <div>
                            <h2>Your order has been delivered!</h2>
                
                            <div style="margin-top:25px;">
                                We hope your enjoy our products and <br>
                                we thank you for taking this journey with us.
                
                                <div style="margin-top: 25px;">
                                    <i>
                                        If you have any questions or concerns feel free to
                
                                        <a href="mailto:matrilinealnerd@gmail.com?subject=Message regarding order #'.$this->order.'">email us</a>
                                    </i>
                                </div>
                            </div>
                
                            <div style="margin-top: 25px;">
                                <small>
                                    Status update for Order# '.$this->order->id.'
                                </small>
                            </div>
                        </div>
                    </center>',
            'cancelled'=>
                '<center>
                            <div>
                                <h3>Your order has been cancelled.</h3>   
                                <div style="margin-top:25px;">
                                    Sorry but your order has been cancelled. <br>
                                    <i>Click the button below for more details.</i>
                                </div>
                    
                                <div style="margin-top: 25px;">
                                    <small>
                                        Status update for Order# '.$this->order->id.'
                                    </small>
                                </div>
                            </div>
                </center>'
        ];
        
        $endings = [
            'shipped'=>"We'll let you know when your order has been delivered.",
            'completed'=>'Thank you for shopping with us.',
            'cancelled'=>"We're sorry that your order didn't work out this time but thank you for shopping with us"
        ];

        $url = 'https://matrilinealnerd.com/public/order-summary/'.$this->order->id;
        $message = $messages[$this->order->status];
        return (new MailMessage)
                    ->subject($subjects[$this->order->status])
                    ->greeting($greetings[$this->order->status])
                    ->line(new HtmlString($message))
                    ->action('View Order', url($url))
                    ->line($endings[$this->order->status]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
