<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class PurchaseConfirmation extends Notification
{
    use Queueable;
    protected $orderNumber;
    protected $payMethod;
    protected $totalPrice;
    protected $address;
    protected $buyer;
    protected $country;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($orderNumber, $totalPrice, 
        $address, $buyer, $payMethod)
    {
        $this->orderNumber = $orderNumber;
        $this->payMethod = $payMethod;
        $this->totalPrice = $totalPrice;
        $this->address = $address;
        $this->buyer = $buyer;
        $this->country =  'United States';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $arr = explode(';',$this->address);
        $greeting = "Great!";
        $url = 'https://matrilinealnerd.com/public/order-summary/'.$this->orderNumber;
        $message = '<div class="text-center">
        <p class="text-left">Thank you for buying our handmade products made with 100% love and attention. <br>
            An update about the status of your order should arrive in <i>1-2 business days</i>.
            <br>
            <center>
                <small>Until then, you can look at the <b><i>information below</i></b>
                for some handy details about your order.
                </small>
            </center>
        </p>
        <center>
            <p style="margin-top: 25px;">
            <div style="font-size: 14pt; color:black;">
                <b>Order Information</b>
            </div>
            <br>
            <div>
                <b>Ship to:</b> <br>
                <div>'.$this->buyer.'</div>
                <div>'.$arr[0].'</div>
                <div>'.$arr[1].', '.$arr[2] .' ' . $arr[3].'</div>
                <div>'.$this->country.'</div>
            </div>
            <br>
                <div>
                <b>Total Price: </b>
                <br>
                $'.number_format($this->totalPrice, 2).' <br> <small><i>Paid with '.
                $this->payMethod.'</i></small>
                </div>
            </p>
        </center>
        </div>';

        $regards = "Thank you for shopping with us.";

        return (new MailMessage)
                    ->greeting($greeting)
                    ->line(new HtmlString($message))
                    ->action('View Order', url($url))
                    ->line($regards);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
