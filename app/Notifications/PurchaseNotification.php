<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PurchaseNotification extends Notification
{
    use Queueable;

    protected $orderNumber;
    protected $price;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($orderNumber, $price)
    {
        $this->orderNumber = $orderNumber;
        $this->price = $price;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = 'https://matrilinealnerd.com/public/order-summary/'.$this->orderNumber;
        $greetings = 'Congrats';
        $message = 'A new purchase has been made with the order number of ' . $this->orderNumber. ' and a total price of $' . $this->price. '.'. 
        ' Please Log into the admin panel to view it now';

        return (new MailMessage)
                    ->Greeting($greetings)
                    ->line($message)
                    ->action('View Order', url($url));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
