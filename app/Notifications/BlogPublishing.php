<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Post;
use Illuminate\Support\HtmlString;

class BlogPublishing extends Notification
{
    use Queueable;
    protected $arr;
    protected $publishing;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($arr, $publishing)
    {
        $this->arr = $arr;
        $this->publishing = $publishing;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = ($this->publishing == true)? "New articles have recently been published" : "Some articles have been unpublished";
        
        $list = '';
        foreach($this->arr as $post) {
            $list.='<li><i>'.$post->title.'</i> written by <b>'.$post->user->name.'</b></li><br>';
        }
    
        $addage = $this->publishing == true? 'published' : 'unpublished';
        $message = '<center>
            <div>
                <h4>The following articles have just been '. $addage. ' on Martilineal Nerd</h4>
                <ul style="list-style: none;">'. $list . '</ul>
            </div>
        </center>';

        return (new MailMessage)
                    ->subject($subject)
                    ->greeting("Just a heads up")
                    ->line(new HtmlString($message));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
