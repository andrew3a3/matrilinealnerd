<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{
    public $incrementing = false;
    public $primaryKey = 'id';

    protected $fillable = [
        'id','cart', 'address', 'user_id', 'name', 'payment_id', 'payment_method', 
        'shipping', 'tax', 'pre_price', 'post_price', 'email', 'status', 'tracking_number'
    ];


    public function user() {
        return $this->belongsTo('App\User');
    }

    public function getCreatedAtAttribute($date) {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    public function getUpdatedAtAttribute($date) {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }


    public static function getOrderId() {return strtolower(str_random(20));}
}