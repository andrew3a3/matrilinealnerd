<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Http\Controllers\ShippingController;

//MAIL
use Illuminate\Support\Facades\Notification;
use App\Notifications\OrderUpdate;

class ManageShipping extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manage:shipping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'AUtomates the management of shipped packages, alerts buyers 
        when the package was delivered and updates its status within the system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where('status', 'shipped')
        ->where('tracking_number', '!=', Null)
        ->get();

        foreach($orders as $order) {
            $result = ShippingController::confirm($order->tracking_number);
            if($result === True) {
                $order->status = 'completed';
                $order->save();
                Notification::route('mail', $order->email)->notify
                (new OrderUpdate($order));
            }
        }
    }
}
