<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Post;
use App\User;
use Carbon\Carbon;

//MAIL
use Illuminate\Support\Facades\Notification;
use App\Notifications\BlogPublishing;


class ManagePosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manage:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automates management of all posts within system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $editors = User::where('editor', true)->where('notifications', true)->get();

        $publishedPosts = [];
        $readyPosts = Post::where('status', 'authorized')
        ->where('publish_on', '>=', Carbon::now())
        ->where('published', false)
        ->where(function($query){
            $query->where('unpublish_on', '<=', Carbon::now())
            ->orWhere('unpublish_on', NULL);})->get();

        foreach($readyPosts as $post) {
            $post->published = true;
            $post->save();

            array_push($publishedPosts, $post);
        }

        //send publish notification
        if(count($publishedPosts) > 0) {
            foreach($editors as $editor) {
                Notification::route('mail', $editor->email)->notify
                    (new BlogPublishing($publishedPosts, true));
            }
        }


        $unpublishedPosts = [];
        $unreadyPosts = Post::where('status', 'authorized')
        ->where('unpublished', '!=', NULL)
        ->where('unpublish_on', '>=', Carbon::now())
        ->where('published', true)->get();

        foreach($unreadyPosts as $post) {
            $post->published = false;
            $post->save();

            array_push($unpublishedPosts, $post);
        }

        //send unpublish notification
        if(count($unpublishedPosts) > 0) {
            foreach($editors as $editor) {
                Notification::route('mail', $editor->email)->notify
                    (new BlogPublishing($unpublishedPosts, false));
            }
        }


        /*$users = User::where('notifications', true)->get();
        $posts = Post::where('status', 'authorized')
        ->where('notify_on_publish', true)
        ->where('notification_sent', false)
        ->where('publish_on', '>=', Carbon::now())
        ->where(function($query){
            $query->where('unpublish_on', '<=', Carbon::now())
            ->orWhere('unpublish_on', NULL);
        })->get();

        foreach($posts as $post) {
            foreach($users as $user) {
                //send email here
            }
        }*/
    }
}