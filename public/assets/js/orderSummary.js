function getTrackingInfo(tracking) {
    let info = $("#delivary-date");
    if (tracking !== "" && tracking !== null) {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });

        $.post(
            "../shipping/tracking",
            {
                tracking: tracking
            },
            function(data) {
                $(info).text(data);
            }
        );
    }
}
