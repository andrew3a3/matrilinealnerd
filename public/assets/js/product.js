const mainImage = document.getElementById('main-image');
const subImages = document.getElementsByClassName('sub-image');

for (var i = 0; i < subImages.length; i++) {
    (function (a) {
        subImages[a].addEventListener('click', () => {
            mainImage.setAttribute('src', subImages[a].src);
        });
    })(i);
}

/*var mainImageTL = new TimelineLite({
    paused: true
});

var descriptionTL = new TimelineLite({
    paused: true
});

mainImageTL.fromTo(images, 3.5, {
    'opacity': '0',
    ease: Power2.easeout
}, {

    'opacity': '1',
    ease: Power2.easeout
});


descriptionTL.fromTo(description, .75, {
    'opacity': '0',
    'margin-top': '200px',
    ease: Power2.easeout
}, {
    'margin-top': '25px',
    'opacity': '1',
    ease: Power2.easeout
});



mainImageTL.play();
descriptionTL.play();*/
