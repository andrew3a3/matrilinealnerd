const homeBtn = document.getElementById('home');
const shopBtn = document.getElementById('shop');
const blogBtn = document.getElementById('blog');
const word = document.getElementById('word');


/*const tween = TweenLite.to(homeBtn, 1, {
    'background-color': '#E08852',
    'text-align': 'center',
    'font-size': '200%',
    'color': 'white'
});*/
const shopTL = new TimelineLite({
    paused: true
});

const homeTL = new TimelineLite({
    paused: true
});

const blogTL = new TimelineLite({
    paused: true
});

shopTL.fromTo(shopBtn, .35, {
    'width': '0%',
    'font-size': '125%',
    'background-color': 'transparent',
    'text-align': 'left',
    'color': '#B4B8BB',
    ease: Power2.easeout
}, {
    'width': '40%',
    'font-size': '200%',
    'background-color': '#C2938D',
    'text-align': 'center',
    'color': 'white',
    ease: Power2.easeout
});


homeTL.fromTo(homeBtn, .35, {
    'width': '0%',
    'font-size': '125%',
    'background-color': 'transparent',
    'text-align': 'left',
    'color': '#B4B8BB',
    ease: Power2.easeout
}, {
    'width': '40%',
    'font-size': '200%',
    'background-color': '#C2938D',
    'text-align': 'center',
    'color': 'white',
    ease: Power2.easeout
});


blogTL.fromTo(blogBtn, .35, {
    'width': '0%',
    'font-size': '125%',
    'background-color': 'transparent',
    'text-align': 'left',
    'color': '#B4B8BB',
    ease: Power2.easeout
}, {
    'width': '40%',
    'font-size': '200%',
    'background-color': '#C2938D',
    'text-align': 'center',
    'color': 'white',
    ease: Power2.easeout
});



shopBtn.addEventListener('click', () => {
    word.innerText = 'Shop';
    shopTL.play();
    homeTL.reverse();
    blogTL.reverse();
});


homeBtn.addEventListener('click', () => {
    word.innerText = 'Home';
    homeTL.play();
    shopTL.reverse();
    blogTL.reverse();
});


blogBtn.addEventListener('click', () => {
    word.innerText = 'Blog';
    blogTL.play();
    shopTL.reverse();
    homeTL.reverse();
});


homeTL.play();
