$('#shipping-rate').val(-1);
$('#shipping-cost').html("TBD");
let final = document.getElementById("final");
final.style.display = "none";

function onZipcodeInput() {
    var zipcode = $('#zipcode').val();
    var tax = 0;
    var state = "";

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $.post('../shipping/lookup', {
            zipcode: zipcode
        },
        function (data) {
            data = JSON.parse(data);
            city = data['city']['0'];
            state = data['state']['0']
            //console.log("City: " + city + ", State: " +
            //  state);
            if (city != undefined && state != undefined) {
                $('#city').val(city);
                $('#state').val(state);

                let url = 'https://api.taxcloud.net/1.0/TaxCloud/lookup';
                const body = {
                    "apiKey": tax_cloud_key,
                    "apiLoginID": tax_cloud_id,
                    "cartID": "uuid",
                    "cartItems": [{
                        "Index": 0,
                        "ItemID": 0,
                        "Price": parseFloat($('#subtotal').text()),
                        "Qty": 1,
                        "TIC": 0,
                    }],
                    "customerID": "1",
                    "deliveredBySeller": false,
                    "destination": {
                        "Address1": " ",
                        "City": $('#city').val(),
                        "State": $('#state').val(),
                        "Zip4": "0000",
                        "Zip5": $('#zipcode').val()
                    },
                    "origin": {
                        "Address1": address_line,
                        "City": address_city,
                        "State": address_state,
                        "Zip4": "0000",
                        "Zip5": address_zipcode
                    }
                };

                // Sending and receiving data in JSON format using POST method
                // GET added tax
                var xhr = new XMLHttpRequest();
                xhr.open("POST", url, true);
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        var json = JSON.parse(xhr.responseText);
                        //console.log(json);

                        if (json.CartItemsResponse[0].TaxAmount != null) {
                            tax = json.CartItemsResponse[0].TaxAmount;
                        }

                        $('#tax-rate').val(tax);

                        //console.log(json.CartItemsResponse[0].TaxAmount);

                        //get shipping cost and calculate all
                        $.post('../shipping/cost', {
                                zipcode: zipcode
                            },
                            function (data) {
                                var shipping = data;
                                if (shipping && shipping !== "") {
                                    var subtotal = parseFloat($('#subtotal').text());
                                    var over25 = subtotal >= 25;

                                    $('#shipping-cost').html((!over25 ? "$" + shipping : "Free"));
                                    if (over25) {
                                        shipping = 0;
                                    }
                                    $('#shipping-rate').val(shipping);

                                    var totalPrice = subtotal + tax + parseFloat(shipping);
                                    totalPrice = totalPrice.toFixed(2);
                                    $('#total').text(totalPrice);
                                    $('#total-price').val(subtotal);
                                    final.style.display = "block";
                                } else {

                                }
                            });
                    }
                };

                var data = JSON.stringify(body);
                xhr.send(data);

            } else {
                $('#shipping-rate').val(-1);
                $('#shipping-cost').html("TBD");
                $('#total').text(parseFloat($('#subtotal').text()).toFixed(2));
                final.style.display = "none";
                return;
            }
        });
}
