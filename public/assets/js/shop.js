let htmlStyles = window.getComputedStyle(document.querySelector("html"));
let rowNum = parseInt(htmlStyles.getPropertyValue("--rowNum"));
let views = $(".shop-view");
let dropdowns = $(".mobile-category-tab");
let mainDropdown = $("#category-dropdown");

const hearts = document.getElementsByClassName("like");

for (var i = 0; i < hearts.length; i++) {
    (function(a) {
        hearts[a].addEventListener("click", () => {
            if (hearts[a].className.includes("fas fa-heart")) {
                hearts[a].className = "far fa-heart like";

                //do unlike function here
            } else {
                hearts[a].className = "fas fa-heart like";

                //do like function here
            }
        });
    })(i);
}

for (var i = 0; i < dropdowns.length; i++) {
    (function(a) {
        dropdowns[a].addEventListener("click", () => {
            var id = dropdowns[a].id.split("-")[0];
            //$(mainDropdown).text(id);
            toggleTag(a);
            toggleView(id);
        });
    })(i);
}

//NAVBAR TAB CONTROL
const tabs = document.getElementsByClassName("category-tab");
const tls = [];
for (var i = 0; i < tabs.length; i++) {
    (function(a) {
        var temp = new TimelineLite({
            paused: true
        });

        temp.fromTo(
            tabs[a],
            0.075,
            {
                "font-size": "90%",
                color: "rgba(162, 118, 139, 1)",
                "font-weight": "normal",
                "background-color": "transparent",
                ease: Power2.easeout
            },
            {
                "font-size": "100%",
                color: "white",
                "font-weight": "bold",
                "background-color": "#943855",
                ease: Power2.easeout
            }
        );

        tls.push(temp);

        tabs[a].addEventListener("click", () => {
            toggleTag(a);
            var id = tabs[a].id.split("-")[0];
            toggleView(id);
        });
    })(i);
}

function toggleTag(index) {
    for (var i = 0; i < tabs.length; i++) {
        if (index == i) {
            tls[i].play();
        } else {
            tls[i].reverse();
        }
    }
}

function toggleView(view) {
    for (var i = 0; i < views.length; i++) {
        views[i].style.display = view == views[i].id ? "block" : "none";
    }

    $(mainDropdown).text(view);
}

toggleTag(0);
toggleView(dropdowns[0].id.split("-")[0]);
