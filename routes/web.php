<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Notification;
use App\Notifications\BlogPublishing;
use App\User;
Use App\Post;

/*Route::get('/test-mail', function (){
    $testEmail = '';
    $posts = Post::all();
    $publishing = true;

    Notification::route('mail', $testEmail)->notify(new BlogPublishing($posts, false));
    return 'Sent';
});*/


Route::get('/', 'PublicController@index')->name('index');
Route::get('post/{id}', 'PublicController@singlePost')->name('singlePost');
Route::get('order-summary/{id}', 'PublicController@orderSummary')->name('order-summary');
Route::get('about', 'PublicController@about')->name('about');
Route::get('contact', 'PublicController@contact')->name('contact');
Route::post('contact', 'PublicController@contactPost')->name('contactPost');
Route::get('profile', 'PublicController@profile')->name('profile')->middleware('verified');
Route::get('usps', 'PublicController@testUsps');
Auth::routes(['verify' => true]);

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/signupconfirmation', 'HomeController@signupConfirmation')->name('signupConfirmation');

Route::prefix('editor')->group(function(){
    Route::get('submitted-posts','AuthorController@submittedPosts')->name('editor.Posts');
}); 

Route::prefix('contributer')->group(function(){
    Route::get('/dashboard','AuthorController@dashboard')->name('cont.Dashboard');
    Route::get('posts','AuthorController@posts')->name('cont.Posts');
    Route::get('post/{id}','AuthorController@getPost')->name('cont.SinglePost');
    Route::get('preview-post/{id}','AuthorController@previewPost')->name('cont.PreviewPost');
    Route::get('createPost','AuthorController@createPost')->name('cont.CreatePost');
    Route::get('comments','AuthorController@comments')->name('cont.Comments');
    Route::post('delete-post/{id}','AuthorController@deletePost')->name('cont.deletePost');
    Route::post('updatePost/{id}', 'AuthorController@updatePost')->name('cont.UpdatePost');
});

Route::prefix('admin')->group(function(){
    Route::get('/dashboard', 'AdminController@dashboard')->name('adminDashboard');

    #content
    Route::get('posts', 'AdminController@posts')->name('admin.posts');
    Route::get('comments', 'AdminController@comments')->name('adminComments');
    Route::get('users', 'AdminController@users')->name('adminUsers');
    
    #Products
    Route::get('products', 'AdminController@products')->name('adminProducts');
    Route::post('product/{id}/delete', 'AdminController@deleteProduct')->name('adminDeleteProduct');
    Route::get('products/new', 'AdminController@newProduct')->name('adminNewProduct');
    Route::post('products/new', 'AdminController@newProductPost')->name('adminNewProduct');
    Route::get('product/{id}', 'AdminController@editProduct')->name('adminEditProduct');
    Route::post('product/{id}', 'AdminController@editProductPost')->name('adminEditProduct');
    Route::post('order/{id}', 'AdminController@editOrder')->name('admin.editOrder');
    Route::get('awaiting-shipment', 'AdminController@awaitingShipment')->name('admin.awaitingShipment');
});

Route::prefix('shop')->group(function() {
    Route::get('/', 'ShopController@index')->name('shop.index');
    Route::get('product/{slug}', 'ShopController@singleProduct')->name('shop.singleProduct');


    Route::get('checkout','ShopController@getCheckout')->name('shop.getCheckout');
    Route::get('paypalCheckout', 'ShopController@getPaypal')->name('shop.getPaypal');
    
    Route::post('process-payment', 'ShopController@processPayment')->name('shop.processPayment');
    Route::get('process-paypal', 'ShopController@processPaypal')->name('shop.processPaypal');

    Route::post('paypal', 'ShopController@paypal')->name('shop.paypal');

    Route::get('confirmation',  'ShopController@confirmation')->name('shop.orderConfirmation');

    //product
    Route::get('addToCart/{id}', 'ProductController@addToCart')->name('shop.addToCart');
    Route::get('shopping-cart','ProductController@getCart')->name('shop.cart');
    Route::get('reduceCart/{id}','ProductController@reduceItemInCart')->name('shop.reduceCart');
    Route::get('removeFromCart/{id}','ProductController@removeFromCart')->name('shop.removeFromCart');
});

Route::prefix('blog')->group(function(){
    Route::get('/', 'BlogController@index')->name('blog.index');
    Route::get('post/{slug}', 'BlogController@singlePost')->name('blog.singlePost');
});


Route::prefix('shipping')->group(function() {
    Route::post('cost', 'ShippingController@shippingCost')->name('shipping.cost');
    Route::post('lookup', 'ShippingController@lookup')->name('shipping.lookup');
    Route::post('tracking', 'ShippingController@tracking')->name('shipping.tracking');
    //Route::post('confirm', 'ShippingController@confirm')->name('shipping.confirm');
    Route::post('tax', 'ShippingController@taxRate')->name('shipping.tax');
});