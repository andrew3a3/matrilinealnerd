@php
use App\Configs\Constants;
$info = Constants::adminUpdates();
@endphp

<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            @if(Auth::check() && Auth::user()->author == true)
            <li class="nav-title">Contributor</li>
            <li class="nav-item nav-dropdown">
                <a href="{{route ('cont.Dashboard')}}"
                    class="nav-link {{Route::currentRouteName() == 'cont.Dashboard'? 'active' : ''}}">
                    <i class="icon icon-speedometer"></i> Dashboard
                </a>
            </li>

            <li class="nav-item nav-dropdown">
                <a href="{{route ('cont.Posts')}}"
                    class="nav-link {{Route::currentRouteName() == 'cont.Posts'? 'active' : ''}}">
                    <i class="icon icon-paper-clip"></i> Posts
                </a>
            </li>

            <li class="nav-item nav-dropdown">
                <a href="{{route ('cont.Comments')}}"
                    class="nav-link {{Route::currentRouteName() == 'cont.Comments'? 'active' : ''}}">
                    <i class="icon icon-book-open"></i> Comments
                </a>
            </li>
            @endif

            @if(Auth::check() && Auth::user()->editor == true)
            <li class="nav-title">Editor</li>
            <li class="nav-item nav-dropdown">
                <a href="{{route ('cont.Dashboard')}}"
                    class="nav-link {{Route::currentRouteName() == 'cont.Dashboard'? 'active' : ''}}">
                    <i class="icon icon-speedometer"></i> Dashboard
                </a>
            </li>

            <li class="nav-item nav-dropdown">
                <a href="{{route ('editor.Posts')}}"
                    class="nav-link {{Route::currentRouteName() == 'editor.Posts'? 'active' : ''}}">
                    <i class="icon icon-paper-clip"></i> Ready for Approval
                    @if($info['awaiting_approval'] > 0)
                    <span class="badge badge-pill badge-danger">{{$info['awaiting_approval']}}</span>
                    @endif
                </a>
            </li>

            @endif

            @if(Auth::check() && Auth::user()->seller == true)
            <li class="nav-title">Seller</li>
            <li class="nav-item nav-dropdown">
                <a href="{{route ('admin.awaitingShipment')}}"
                    class="nav-link {{Route::currentRouteName() == 'admin.awaitingShipment'? 'active' : ''}}">
                    <i class="icon icon-rocket"></i> Awaiting Shipment
                    @if($info['awaiting_shipment'] > 0)
                    <span class="badge badge-pill badge-danger">{{$info['awaiting_shipment']}}</span>
                    @endif
                </a>
            </li>

            <li class="nav-item nav-dropdown">
                <a href="{{route ('adminProducts')}}"
                    class="nav-link {{Route::currentRouteName() == 'adminProducts'? 'active' : ''}}">
                    <i class="icon icon-basket-loaded"></i> Products
                </a>
            </li>


            <li class="nav-item nav-dropdown">
                <a href="{{route ('adminProducts')}}"
                    class="nav-link {{Route::currentRouteName() == 'adminProducts'? 'active' : ''}}">
                    <i class="icon icon-check"></i> Promos
                </a>
            </li>

            @endif

            @if(Auth::check() && Auth::user()->admin == true)
            <li class="nav-title">Admin</li>

            <li class="nav-item nav-dropdown">
                <a href="{{route ('adminDashboard')}}"
                    class="nav-link {{Route::currentRouteName() == 'adminDashboard'? 'active' : ''}}">
                    <i class="icon icon-speedometer"></i> Dashboard
                </a>
            </li>


            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link {{Route::currentRouteName() == 'adminUsers'? 'active' : ''}}">
                    <i class="icon icon-people"></i> Users
                </a>
            </li>
            @endif

            <!-- <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-target"></i> Layouts <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="layouts-normal.html" class="nav-link">
                            <i class="icon icon-target"></i> Normal
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="layouts-fixed-sidebar.html" class="nav-link">
                            <i class="icon icon-target"></i> Fixed Sidebar
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="layouts-fixed-header.html" class="nav-link">
                            <i class="icon icon-target"></i> Fixed Header
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="layouts-hidden-sidebar.html" class="nav-link">
                            <i class="icon icon-target"></i> Hidden Sidebar
                        </a>
                    </li>
                </ul>
            </li>



            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-energy"></i> UI Kits <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="alerts.html" class="nav-link">
                            <i class="icon icon-energy"></i> Alerts
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="buttons.html" class="nav-link">
                            <i class="icon icon-energy"></i> Buttons
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="cards.html" class="nav-link">
                            <i class="icon icon-energy"></i> Cards
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="modals.html" class="nav-link">
                            <i class="icon icon-energy"></i> Modals
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="tabs.html" class="nav-link">
                            <i class="icon icon-energy"></i> Tabs
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="progress-bars.html" class="nav-link">
                            <i class="icon icon-energy"></i> Progress Bars
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="widgets.html" class="nav-link">
                            <i class="icon icon-energy"></i> Widgets
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-graph"></i> Charts <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="chartjs.html" class="nav-link">
                            <i class="icon icon-graph"></i> Chart.js
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="forms.html" class="nav-link">
                    <i class="icon icon-puzzle"></i> Forms
                </a>
            </li>

            <li class="nav-item">
                <a href="tables.html" class="nav-link">
                    <i class="icon icon-grid"></i> Tables
                </a>
            </li>

            <li class="nav-title">More</li>

            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-umbrella"></i> Pages <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="blank.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Blank Page
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="login.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Login
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="register.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Register
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="invoice.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Invoice
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="404.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> 404
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="500.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> 500
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="settings.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Settings
                        </a>
                    </li>
                </ul>
            </li>-->
        </ul>
    </nav>
</div>