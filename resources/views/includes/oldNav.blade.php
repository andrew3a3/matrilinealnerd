<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
    <a href="#" class="navbar-brand">Matrilineal Nerd</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarText">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('shop.index')}}">Shop</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('shop.cart') }}">
                    <i class="fa fa-shopping-cart" aria-hidden="true" href="{{ route('shop.cart') }}"></i>
                    Shopping Cart
                    <span class="badge">
                        <?php use App\Http\Controllers\ProductController;?>
                        @if(ProductController::hasCart())
                        <span class="badge">{{Session::get('cart')->totalQty}}</span>
                        @endif
                    </span>
                </a>
            </li>

            @if(Auth::check())
            <!--User Dropdown-->
            <li class="nav-item">
                <div class="dropdown">
                    <i class="far fa-user-circle dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown">
                        Account
                    </i>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <div class="dropdown-header" type="button">
                            {{Auth::user()->name}}
                        </div>
                        <a class="dropdown-item" href="{{ route('profile')}}">Profile</a>
                        <form method="POST" id="logout-form" action="{{route('logout')}}">@csrf</form>
                        <a class="dropdown-item" href="#" onclick="document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                    </div>
                </div>
            </li>
            @if(Auth::user()->admin == true)
            <li class="nav-item">
                <a class="nav-link" href="{{route('adminDashboard')}}">Dashboard</a>
            </li>
            @endif
            @else
            <li class="nav-item">
                <a class="nav-link" href="{{route('login')}}">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('register')}}">Register</a>
            </li>
            @endif
        </ul>
    </div>
</nav>