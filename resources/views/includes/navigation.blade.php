<link rel="stylesheet" href="{{asset('assets/css/nav.css')}}" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="{{ asset('site-images/favicon.png') }}">
<!-- Navigation -->
<div class="container-fluid" id="heading">
    <nav class="navigation fixed-top informative-header">
        <div class="row">
            <div class="col">
                <div class="dropdown" style="font-size:18pt;">
                    <button id="expand" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bars"></i>
                    </button>

                    <div class="dropdown-menu emphasis" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{route('shop.index')}}">Shop</a>
                        <a class="dropdown-item" href="{{route('blog.index')}}">Blog</a>
                        @if(Auth::check())
                        <a class="dropdown-item" href="{{ route('profile')}}">Profile</a>
                        <form method="POST" id="logout-form" action="{{route('logout')}}">@csrf</form>
                        <a class="dropdown-item" href="#" onclick="document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        @else
                        <a class="dropdown-item" href="{{route('login')}}">Login</a>
                        <a class="dropdown-item" href="{{route('register')}}">Register</a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col links" style="font-size:14pt;">
                <div class="row">
                    <div class="col"><a class="nav-link" href="{{route('shop.index')}}">Shop</a></div>
                    <div class="col"><a class="nav-link" href="{{route('blog.index')}}">Blog</a></div>
                </div>
            </div>
            <div class="col">
                <a href="{{route('index')}}">
                    <img id="logo" src="{{asset('site-images/logo.png')}}" alt="logo">
                </a>
            </div>
            <div class="col links" style="font-size:14pt;">
                @if(Auth::check())
                <div class="row">
                    <div class="col">
                        <a class="nav-link" href="{{ route('profile')}}">Profile</a>
                    </div>
                    <div class="col">
                        <form method="POST" id="logout-form" action="{{route('logout')}}">@csrf</form>
                        <a class="nav-link" href="#" onclick="document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col"><a class="nav-link" href="{{route('login')}}">Login</a></div>
                    <div class="col"><a class="nav-link" href="{{route('register')}}">Register</a></div>
                </div>
                @endif
            </div>
            <div class="col">
                <div id="cart">
                    @if(Auth::check() && Auth::user()->admin == true)
                        <a href="{{route('adminDashboard')}}">
                            <i class="fas fa-user-cog fa-1x"></i>
                        </a>
                    @endif
                    <a href="{{ route('shop.cart') }}">
                        <i class=" fas fa-shopping-cart fa-1x"></i>
                        <span id="badge">
                            <?php use App\Http\Controllers\ProductController;?>
                            @if(ProductController::hasCart())
                            <span class="badge badge-pill badge-dark">{{Session::get('cart')->totalQty}}</span>
                            @endif
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </nav>
</div>