<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145822552-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-145822552-1');
    </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    @yield('extra-meta')
    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <script src="https://kit.fontawesome.com/bcc76ca434.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Cardo|Contrail+One|Roboto+Condensed&display=swap"
        rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Darker+Grotesque|Marvel|Abel|Advent+Pro&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Cardo&display=swap" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/CSSPlugin.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenLite.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineLite.min.js"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}" rel="stylesheet" type="text/css">
    @include('includes.navigation')
</head>

<body>
    @yield('content')
    @yield('scripts')
    <!-- Footer -->
    <footer class="font-small">

        <!-- Footer Elements -->
        <div class="container">
            <hr>
            <!-- Grid column -->
            <div>
                    <h4 class="emphasis">Follow Us</h4>
                    <br>
                    <div class="mb-5 flex-center">
                        <a id="social" href="https://twitter.com/matrilinealnerd">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>

                        <!--<a id="social" href="#">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>-->

                        <a id="social" href="https://www.instagram.com/matrilinealnerd">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </div>
                </div>
        </div>
        <!-- Footer Elements -->

        <!-- Copyright -->
        <div class=" text-center">© 2019 Copyright:
            <a href="https://matrilinealnerd.com"> MatrilinealNerd.com</a>
        </div>
        <!-- Copyright -->

    </footer>
    <!-- Footer -->
</body>

</html>