<!DOCTYPE html>
<html lang="en">

<head>
    @yield('analytics')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    @yield('extra-meta')
    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <script src="https://kit.fontawesome.com/bcc76ca434.js"></script>
    <!-- Navigation -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="https://kit.fontawesome.com/bcc76ca434.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet'
        type='text/css'>
    <link
        href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
        rel='stylesheet' type='text/css'>


    <link rel="stylesheet" href="{{asset('assets/css/master.css')}}" rel="stylesheet" type="text/css">
    @include('includes.navigation')
</head>

<body>
    @yield('content')
    @yield('scripts')


    <!-- Footer -->
    <div class="container">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-10 mx-auto">
                        <ul class="list-inline text-center social">
                            <li class="list-inline-item">
                                <a href="https://twitter.com/matrilinealnerd">
                                    <span class="fa-stack fa-lg">
                                        <i class="fas fa-circle fa-stack-2x"></i>
                                        <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <span class="fa-stack fa-lg">
                                        <i class="fas fa-circle fa-stack-2x"></i>
                                        <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://www.instagram.com/matrilinealnerd/">
                                    <span class="fa-stack fa-lg">
                                        <i class="fas fa-circle fa-stack-2x"></i>
                                        <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <center>
                            <p class="copyright text-muted">
                                Copyright &copy; Matrilineal Nerd 2019
                            </p>
                        </center>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>


<!--CART -->
<div class="wrapper row">
    <div class="col-8">
        <div class="card">
            <div class="card-header text-center">
                <h2><b>Shopping Cart</b></h2>
            </div>
            <div class=" card-body">
                <ul class="list-group" style="list-style-type:none;">
                    @foreach($products as $product)
                    <!--Indiviual item-->
                    <li>
                        <div class="row">
                            <div class="col">
                                <img class="cart-image" src="{{asset($product['item']['image'])}}"
                                    alt="{{$product['item']['title']}}" />
                            </div>
                            <div class="col">
                                <ul class="list-group" style="list-style-type:none;">
                                    <li class="font-weight-bold">
                                        {{$product['item']['title']}}
                                    </li>
                                    <li class="font-weight-normal">
                                        <small><i>${{number_format($product['price'], 2)}}</i></small>
                                    </li>
                                    <li class="font-weight-light">
                                        <small><i>Currently available</i></small>
                                    </li>
                                </ul>
                            </div>
                            <div class="col text-center">
                                <div class="row justify-content-md-center">
                                    <span class="col">
                                        <a href="{{ route('shop.reduceCart', $product['item']['id']) }}"
                                            disabled="disabled" data-type="minus" data-field="quant[1]">
                                            <i class="fas fa-caret-left"></i>
                                        </a>
                                    </span>
                                    <span class="col">
                                        <span class="font-weight-light">
                                            {{$product['qty']}}
                                        </span>
                                    </span>
                                    <span class="col">
                                        <a href="{{ route('shop.addToCart', $product['item']['id']) }}" data-type="plus"
                                            data-field="quant[1]">
                                            <i class="fas fa-caret-right"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm text-right">
                                @php
                                $totalProductPrice = $product['price'] * $product['qty'];
                                @endphp
                                <b>${{number_format($totalProductPrice, 2)}} </b>
                            </div>

                            <div class="col text-right">
                                <a href="{{ route('shop.removeFromCart', $product['item']['id']) }}">
                                    <i class="fas fa-minus-circle"></i>
                                </a>
                            </div>
                        </div>
                        <hr />
                    </li>
                    <!--End Individual item-->
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card">
            <div class="card-header text-center">
                <h2><b>Order Summary</b></h2>
            </div>
            <div class="card-body">
                <div class="row" style="margin-top: 10px;">
                    <div class="col-8 text-left">
                        <b>Subtotal</b>
                    </div>
                    <div class="col text-center">
                        ${{number_format($totalPrice, 2)}}
                    </div>
                    <div class="col-4 text-right">
                        <i>${{number_format($totalPrice, 2)}}</i>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="{{ route('shop.getCheckout') }}" type="button" class="btn btn-block">Proceed to
                    checkout</a>
                <div style="text-align: center;margin-top: 15px; margin-bottom:15px;">
                    <b>or</b>
                </div>
                <div style="text-align: center;">
                    <a href="{{route('shop.getPaypal')}}" type="button">
                        <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-medium.png"
                            alt="Check out with PayPal" />
                    </a>
                </div>
            </div>
        </div>

        <div style="text-align: center; margin-top: 20px;">
            <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png"
                alt="Buy now with PayPal" width="65%" />
        </div>
    </div>
</div>
</div>


<!-- PRODUCT -->
<!-- Page Header -->
<header class="masthead" style="background-image: url('{{asset('assets/img/home-bg.jpg')}})">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>{{$product->title}}</h1>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="container">
    <div class="row">
        <div class="col-md-5">
            <img src="{{ asset($product->image) }}" alt="">
        </div>
        <div class="col-md-6">
            <h2>{{$product->title}}</h2>
            <hr>
            {{$product->description}}
            <hr>
            <b>{{ $product->price }}</b>
            <br>
            <a href="{{ route('shop.addToCart', $product->id) }}" class="btn btn-primary">Add to Cart</a>
        </div>
    </div>
</div>


<!--SHOP INDEX -->
<div class="wrapper">
    @foreach($products as $product)
    <div class="grid-item">
        <a href="{{ route('shop.singleProduct', $product->id) }}">
            <img class="product-image" src="{{ asset($product->image) }}" width="150px" alt="{{$product->title}}">
        </a>

        <div class="product-info">
            <a href="{{ route('shop.singleProduct', $product->id) }}">
                <div class="product-title">
                    <h5>{{$product->title}}</h5>
                </div>
                <div class="product-price"><i>
                        ${{$product->price}}
                    </i></div>
            </a>
            <div class="row">
                <div class="col-7">
                    <a href="{{ route('shop.addToCart', $product->id) }}"
                        class="btn btn-outline-primary btn-sm product-add">Add to cart</a>
                </div>
                <div class="col">
                    <i class="far fa-heart like"></i>
                    <i class="fas fa-share-alt share"></i>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>