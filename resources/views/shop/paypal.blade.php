@extends('layouts.master')
@section('extra-meta')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('title')
Matrilineal Nerd - Checkout
@endsection

@section('content')
<div class="container mainContent">
    <form id="nonce-form" action="paypal" method="post" onsubmit="return validateForm()">
        <div class="checkout-grid">
            <div>
                <div class="card" id="form">
                    <div class="card-body">
                        <h5 class="card-title emphasis-light">
                            <strong>Payment Details</strong>
                        </h5>

                        <!--Shipping Information-->
                        <label class="informative-header" for="shipping">Shipping Information</label>
                        <div class="row" id="shipping">
                            <!--Name-->
                            <div class="input-group mb-3">
                                <input name="name" type="text" placeholder="Full Name" class="form-control" required />
                            </div>
                            <!--Email-->
                            <div class="input-group mb-3">
                                <input name="email" type="email" placeholder="Email" class="form-control" required />
                            </div>
                            <!--Street Address-->
                            <div class="input-group mb-3">
                                <input name="address" id="address-input" type="text" placeholder="Street Address" class="form-control" required />
                            </div>
                            <!--City, State, Zipcode-->
                            <div class="row">
                                <div class="col-6">
                                    <div class="input-group mb-3">
                                        <input id="city" name="city" type="text" placeholder="City" class="form-control" required />
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group mb-3">
                                        <input id="state" name="state" type="text" placeholder="State" class="form-control" required />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="input-group mb-3">
                                        <input id="zipcode" name="zipcode" type="text" placeholder="Zipcode" onchange="onZipcodeInput();" class="form-control" required />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br />
                    </div>
                </div>
            </div>
            <div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title emphasis-light">
                            <strong>Order Summary</strong>
                            <br>
                        </h5>
                        <ul class="list-group list-group-flush" style="list-style-type:none;">
                            @foreach ($items as $item)
                            @if($item['qty'] > 0)
                            <li class="">
                                <div class="row">
                                    <div class="col-2">
                                        <img src="{{asset($item['item']['image'])}}" width="30" alt="" />
                                    </div>

                                    <div class="col-6 text-center informative">
                                        <i>{{$item['qty']}} x {{$item['item']['title']}}</i>
                                    </div>
                                    <div class="col text-right">
                                        <strong class="informative-header">${{number_format($item['item']['price'],2)}}
                                            <small><i class="informative-light">ea.</i></small></strong>
                                    </div>
                                </div>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                    @php
                    $shipping = 0;
                    $taxRate = 0;

                    /*if(Auth::check()) {
                    $total = $total - ($total * .15);
                    }*/

                    if(TRUE) {
                    $total = $total - ($total * .20);
                    }


                    $accountedTotal = $total + ($total * ($taxRate / 100)) + $shipping;
                    @endphp

                    <div class="card-footer informative">
                        <div class="row"> <i class="informative-header">Subtotal: &nbsp; </i> $
                            <span id="subtotal">{{ number_format($total, 2) }}</span></div>
                        <div class="row"><i class="informative-header">Shipping: &nbsp;</i><span id="shipping-cost" class="informative"></span></div>
                        <div class="row informative-header">
                            <div id="final">
                                <i>Total <small>(Including Tax and shipping)</small>: &nbsp;</i> $
                                <span id="total">
                                    {{number_format($accountedTotal, 2)}}
                                </span>
                            </div>
                        </div>
                    </div>
                    <br />

                    <center>
                        <button id="paypal-charge" type="submit">
                            <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-large.png" alt="Buy now with PayPal" />
                        </button>
                    </center>
                </div>
            </div>
            <input type="hidden" id="total-price" name="total-price" />
            <input type="hidden" id="shipping-rate" name="shipping-rate" />
            <input type="hidden" id="tax-rate" name="tax-rate" />
            @csrf
    </form>

    <div class="text-center">
        <small class="informative-header" style="font-size: 7pt !important;">Shipped With</small>
        <br>
        <img src="https://i.imgur.com/hafqAuG.jpg" width="128px" alt="">
    </div>
    <br>
    <!-- Modal -->
    <div id="error-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title emphasis ">Looks like we have a problem</h4>
                </div>
                <div class="modal-body text-center" style="font-weight:300;">
                    <p>
                        Unfortunately there was a problem processing your order.
                        Either your shipping information was incorrect or there was an issue with the location you are
                        trying
                        to ship to.
                        <br> <br>
                        <small>Please be aware that right now we only ship within the <b><i>United
                                    States</i></b></small>
                    </p>
                    <br>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span style="color:blue;">OK</span></button>
                </div>
            </div>

        </div>
    </div>

    <style>
        form {
            padding-left: 2%;
            padding-right: 2%;
        }

        small {
            font-weight: 400;
        }
    </style>



</div>

@endsection

@section('scripts')
<script>
    window.address_line = <?php echo "\"";
                            echo ($_ENV["ADDRESS_LINE"]);
                            echo "\""; ?>;
    window.address_city = <?php echo "\"";
                            echo ($_ENV["ADDRESS_CITY"]);
                            echo "\""; ?>;
    window.address_state = <?php echo "\"";
                            echo ($_ENV["ADDRESS_STATE"]);
                            echo "\""; ?>;
    window.address_zipcode = <?php echo "\"";
                                echo ($_ENV["ADDRESS_ZIPCODE"]);
                                echo "\""; ?>;
    window.tax_cloud_id = <?php echo "\"";
                            echo ($_ENV["TAX_CLOUD_API_ID"]);
                            echo "\""; ?>;
    window.tax_cloud_key = <?php echo "\"";
                            echo ($_ENV["TAX_CLOUD_API_KEY"]);
                            echo "\""; ?>;
</script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/css/checkout.css')}}">
<script type="text/javascript" src="{{asset('assets/js/checkout.js')}}"></script>

<script type="text/javascript">
    function validateForm() {
        var shipping = document.getElementById('shipping-rate').value;

        if (shipping == null || shipping < 0) {
            $("#error-modal").modal("show");
            return false;
        }

        return true;
    }
</script>
@endsection