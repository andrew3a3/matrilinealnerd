@extends('layouts.master')

@section('title')
Matrilineal Nerd - Shopping Cart
@endsection


@section('content')
@if(Session::has('cart'))
<div class="container">
    <div class="grid">
        <div class="card">
            <div class="card-header text-center emphasis">
                <h2><b>Shopping Cart</b></h2>
            </div>
            <div class="card-body">
                <ul class="list-group" style="list-style-type:none;">
                    @foreach($products as $product)
                    <li style="margin-bottom:10px;">
                        <div class=" item">
                            <div class="text-left">
                                <img src="{{asset($product['item']['image'])}}" alt="{{$product['item']['title']}}"
                                    width="65px">
                            </div>
                            <div class="text-center item-info">
                                <div class="informative-header">
                                    <b>{{$product['item']['title']}}</b>
                                </div>
                                <div class="informative">${{number_format($product['price'], 2)}}</div>
                                <div class="informative-light">
                                    <small>Available</small>
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="modifier">
                                    <div>
                                        <a href="{{ route('shop.reduceCart', $product['item']['id']) }}"
                                            disabled="disabled" data-type="minus" data-field="quant[1]">
                                            <i class="fas fa-minus fa-xs"></i>
                                        </a>
                                    </div>
                                    <div>
                                        <span class="informative-light">
                                            {{$product['qty']}}
                                        </span>
                                    </div>
                                    <div>
                                        <a href="{{ route('shop.addToCart', $product['item']['id']) }}" data-type="plus"
                                            data-field="quant[1]">
                                            <i class="fas fa-plus fa-xs"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right informative-header">
                                @php
                                $totalProductPrice = $product['price'] * $product['qty'];
                                @endphp
                                <b>${{number_format($totalProductPrice, 2)}}</b>
                            </div>
                            <div class="text-right">
                                <a href="{{ route('shop.removeFromCart', $product['item']['id']) }}">
                                    <i class="fas fa-minus-circle" style="color: crimson !important"></i>
                                </a>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div>
            <div class="card">
                <div class="card-header text-center emphasis">
                    <h2><b>Order Summary</b></h2>
                </div>
                <div class="card-body">
                    <div class="subtotal informative-header">
                        <div><b>Subtotal</b></div>
                        <div class="text-center informative-light">
                            @if(Auth::check())
                            <span>${{number_format($totalPrice, 2)}}</span>
                            @endif
                        </div>
                        @php
                        //$discount = (Auth::check())? $totalPrice - ($totalPrice * .15) : $totalPrice;
                        $discount = $totalPrice - ($totalPrice * .2);
                        @endphp
                        <div class="text-right informative"><i>${{number_format($discount, 2)}}</i></div>
                    </div>

                    <!-- @if(Auth::check())
                    <div class="savings">
                        <div class="text-left"><b>Promo Code</b></div>
                        <div class="text-right">SMSAVE20</div>
                    </div>
                    @endif -->

                    @if(True)
                    <div class="savings">
                        <div class="text-left"><b>Promo Code</b></div>
                        <div class="text-right">SMSAVE20</div>
                    </div>
                    @endif
                </div>
                <div class="card-footer"></div>
            </div>
            <div id="payment-methods">
                <a href="{{ route('shop.getCheckout') }}" type="button"
                    class="btn btn-dark btn-lg btn-block informative-header">Pay with
                    Card</a>
                <div style="text-align: center;margin-top: 15px; margin-bottom:15px;">
                    <b>or</b>
                </div>
                <div class="text-center">
                    <a href="{{route('shop.getPaypal')}}" type="button">
                        <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-large.png"
                            alt="Check out with PayPal" />
                    </a>
                </div>

                <div class="text-center" style="margin-top: 20px;">
                    <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png"
                        alt="Buy now with PayPal" width="65%" />
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="container">
    <div class="row">
        <div class="col text-center">
            <h1><strong>No items in cart</strong></h1>
        </div>
    </div>
</div>
@endif
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/css/cart.css')}}" rel="stylesheet" type="text/css">
@endsection