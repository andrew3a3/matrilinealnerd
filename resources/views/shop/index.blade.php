@extends('layouts.master')

@section('title')
Matrilineal Nerd - Shop
@endsection

@section('content')
<!-- Page Header -->
<!-- <header class="masthead" style="background: #FEC4C2;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading text-center">
                    <strong>
                        Save 20% on all orders
                    </strong>
                    <br>
                    <strong style="font-size:1.75rem; padding-bottom:5px;">Free Shipping on orders $25 and up</strong>
                </div>
            </div>
        </div>
    </div>
</header> -->

<div class="container-fluid">
    <div class="text-center container-fluid" id="item-dropdown">
        <div class="dropdown emphasis">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="category-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{$categories[0]}}
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                @foreach($categories as $category)
                <button class="dropdown-item mobile-category-tab informative-header" type="button" id="{{$category}}-item">{{$category}}</button>
                @endforeach
            </div>
        </div>
    </div>
    <div id="shop-body">
        <div class="navbar emphasis">
            <div id="tabs" class="row">
                @foreach($categories as $category)
                <div class="col">
                    <span class="category-tab informative-header" id="{{$category}}-tab">{{$category}}</span>
                </div>
                @endforeach
            </div>
        </div>
        @php
        $count = 0;
        @endphp
        @foreach($views as $view)
        <div class="shop-view" id="{{$categories[$count]}}">
            @if(count($view) > 0)
            <div class="wrapper">
                @foreach($view as $product)
                <div class="grid-item">
                    <div style="margin-bottom:40x;">
                        <!--<div class="grid-decoration"></div>-->
                        <a href="{{ route('shop.singleProduct', $product->slug) }}">
                            <img class="product-image" src="{{ asset($product->image) }}" width="250px" alt="{{$product->title}}">
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('shop.singleProduct', $product->slug) }}">
                            <div class="product-title informative-header">
                                <b style="font-size:14pt;">{{$product->title}}</b>
                            </div>
                            <div class="product-price informative-light" style="margin-top:5px; font-size:13pt;">
                                ${{$product->price}}
                            </div>
                        </a>
                    </div>
                    <div class="preview-bottom">
                        <div class="text-center">
                            <a href="{{ route('shop.addToCart', $product->id) }}" class="btn btn-outline-primary btn-sm product-add">Add to cart</a>
                        </div>
                        <!--<div class="text-left">
                                    <i class="far fa-heart preview-intr like"></i>
                                    <i class="fas fa-share-alt preview-intr"></i>
                                </div>-->
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <div class="text-center" style="margin-top:25px;">
                <h2>No {{$categories[$count]}} currently available</h2>
            </div>
            @endif
            <div class="text-center">
                {{ $view->links() }}
            </div>
        </div>
        @php
        $count++;
        @endphp
        @endforeach
    </div>
</div>
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/css/dynamic-grid.css')}}">
<script src="{{asset('assets/js/shop.js')}}"></script>
@endsection