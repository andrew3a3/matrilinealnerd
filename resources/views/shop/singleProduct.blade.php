@extends('layouts.master')

@section('title')
Matrilineal Nerd - {{$product->title}}
@endsection

@section('content')
<style>
    body {
        margin-top: 50px !important;
    }
</style>
<div class="container">
    <div class="product-view">
        <div class="text-left">
            <div id="back-text">
                <button class="maroon" onclick="goBack()">
                    <i class="fas fa-chevron-left"></i>
                    <b class="emphasis">Back</b>
                </button>
            </div>
            <br>
            <!--<div id="mini-images" class="text-right">
                <div><img class="sub-image" src="{{ asset($product->image) }}" width="75px" alt="">
                </div>
                <div><img class="sub-image" src="{{ asset($product->image) }}" width="75px" alt="">
                </div>
                <div><img class="sub-image" src="{{ asset($product->image) }}" width="75px" alt="">
                </div>
            </div>-->
        </div>
        <div class="text-center">
            <div id="mobile-product-title" class="text-center informative-header">
                <h2>{{$product->title}}</h2>
            </div>
            <img id="main-image" src="{{ asset($product->image) }}" width="400px"  alt="">
        </div>
        <div class="text-center product-info">
            <h2 class="informative-header" id="product-title">{{$product->title}}</h2>
            <br>
            <div class="product-description informative description-text"> 
            </div>
            <div class="product-interaction" id="top-interaction">
                <div class="text-left maroon informative-header"><strong>${{ $product->price }}</strong></div>
                <div class="text-right maroon">
                <a href="{{ route('shop.addToCart', $product->id) }}" class="btn btn-outline-dark btn-md"
                    id="add-product">Add to Cart</a>
                </div>
            </div>
        </div>
    </div>
    <div id="mobile-product-info">
        <div class="description-text informative text-center"></div>
        <div class="text-center maroon informative-header">
            <h4>
                ${{ $product->price }}
            </h4>
        </div>
        <br>
        <div class="row">
            <div class="col text-center maroon">
                <a href="{{ route('shop.addToCart', $product->id) }}" class="btn btn-outline-dark btn-md"
                    id="add-product">Add to Cart</a>
            </div>
            <!--<div class="col text-center maroon">
                <i class="far fa-heart fa-2x"></i>
                <i class="fas fa-share-alt fa-2x"></i>
            </div>-->
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('assets/js/product.js')}}"></script>

@php
$js_safe_description = json_encode($product->description)
@endphp
<script>
function goBack() {
    window.history.back();
}

window.onload = function() {
    let description = <?php echo $js_safe_description; ?>;
    let descriptions = $('.description-text');
    for(var i = 0; i < descriptions.length; i++) {
        (function(a) {
            $(descriptions[a]).html(description);
        })(i);
    }    
}
</script>
@endsection