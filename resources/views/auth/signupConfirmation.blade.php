@extends('layouts.auth')

@section('title')
Matrilineal Nerd - Signup Confirmation
@endsection

@section('analytics')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145822552-1"></script>
<script>
window.dataLayer = window.dataLayer || [];

function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', 'UA-145822552-1');
</script>

@endsection

@php
header("refresh:5;url=https://matrilinealnerd.com/public/");
@endphp

@section('content')
<div class="container">
    <h1>Thank you for joining the Matrilineal Nerd Squad!</h1>
    <br>
    You will be redirected in a few seconds or <a href="{{route('dashboard')}}">Click here</a> to redirect now
</div>
@endsection