@extends('layouts.master')

@section('extra-meta')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('title')
Matrilineal Nerd - Order Summary
@endsection

@section('content')

@php
if($order != null) {
    $address = explode(";", $order->address);
    $method = $order->payment_method == 'paypal'? 'paypal' : 'card';
    $shipping = (floatval($order->shipping) == 0)? 'Free': "$" . number_format($order->shipping, 2);
    $totalPrice = $order->post_price + $order->shipping + $order->tax;
    $status = strcmp($order->status,'completed') ==  0? 'Delivered': $order->status;
}
@endphp

<div class="container" style="padding-top:25px;">
        @if($order != null) 
        <div class="text-center">
            <h1 class="informative-header">Order</h1>
            <div class="informative" style="font-size: 16pt;">Order# {{$order->id}}</div>
        </div>
        <div class="row text-center" id="shipping-update">
            <div class="col">
                <h5 class="emphasis-light" style="margin-bottom: -10px;">Order Status</h5>
                <div class="informative">
                    <p>
                        @php
                        $color = "#999999";
                        switch($order->status) {
                            case 'shipped':
                            $color = '#FFC576';
                            break;

                            case 'completed':
                            $color = '#57BC91';
                            break;

                            case 'cancelled':
                            $color = '#DC143C';
                            break;
                        }
                        @endphp
                        
                        <div class="informative-header" style="color: {{$color}}; font-size: 24pt !important;">
                            {{ucfirst($status)}}
                        </div>
                        <br>
            
                        <!-- Tracking Number -->
                        @if($order->tracking_number != null)
                        <span id="delivary-date">
                        </span>
                        <br>
                        <span class="informative" style="font-size: 9pt;">
                            @if(Auth::check() && Auth::user()->seller == true|| 
                            (Auth::check() && Auth::user()->id == $order->user_id && $order->user_id > 0))
                                <script type="text/javascript" src="{{asset('assets/js/orderSummary.js')}}"></script>
                                <span id="tracking-info">Tracking #:{{$order->tracking_number}}</span>
                                <script>
                                    getTrackingInfo('{{$order->tracking_number}}');
                                </script>
                            @else
                            <a href="{{route('login')}}"> Sign In to view tracking #</a>
                            @endif
                        </span>
                        @endif
                    </p>
                </div>
                @if($order->tracking_number != null)
                <div>
                    <small class="informative-header" style="font-size: 7pt !important;">Shipped With</small>
                    <br>
                    <img src="https://i.imgur.com/hafqAuG.jpg" width="128px" alt="">
                </div>
                @endif
            </div>
        </div>
        <div class="row text-center" style="margin-top: 2em;">
            <div class="order-grid col">
                <div class="card">
                    <div class="card-header emphasis brand-color">
                        Payer Information
                    </div>
                    <div class="card-body text-center order-card">
                        <div>
                            <div class="emphasis">
                                <h5>Shipping Details</h5>
                            </div>
                            @if(Auth::check() && Auth::user()->seller == true || 
                                (Auth::check() && Auth::user()->id == $order->user_id && $order->user_id > 0))
                            <div><strong>{{$order->name}}</strong></div>
                            <div>
                                <span>{{$address[0]}}</span>
                                <br>
                                <span>{{$address[1]}}, {{$address[2]}} {{$address[3]}}</span>
                                <br>
                                <span><small>United States</small></span>
                            </div>
                            @else
                            <div>
                                <span>
                                    <a href="{{route('login')}}">Sign In</a>
                                </span>
                                <br>
                                <span><b>or</b></span>
                                <br>
                                <span><small>Check email for a purchase confirmation to view shipping information</small></span>
                            </div>
                            @endif
                        </div>
                        <div>
                            <div class="emphasis">
                                <h5>Payment Details</h5>
                            </div>
                            <div><strong>Subtotal&nbsp;</strong></div>
                            <div>
                                @if($order->pre_price != $order->post_price)
                                <span>
                                    <strike><i>&nbsp;${{number_format($order->pre_price, 2)}}&nbsp;</i></strike>
                                </span>
                                @endif
                                <span>${{number_format($order->post_price, 2)}}</span>
                            </div>

                        </div>
                        <div>
                            <div><strong>Shipping&nbsp;</strong></div>
                            <span><i>{{$shipping}}</i></span>
                        </div>
                        <div>
                            <div><strong>Tax&nbsp;</strong></div>
                            <span><i>${{number_format($order->tax, 2)}}</i></span>
                        </div>
                        <div>
                            <span>
                                <strong>Total</strong>
                                <br>
                                ${{number_format($totalPrice, 2)}}
                            </span>
                        </div>
                        <div>
                            <small>
                                Payed with {{$method}}
                            </small>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header emphasis brand-color">
                        Order Information
                    </div>

                    <div class="card-body text-center order-card">
                        @foreach($order->cart->items as $item)
                        <div class="row emphasis-light">
                            <div class="col">
                                <img src="{{asset($item['item']['image'])}}" width="45px;" height="45px;" alt=""
                                    style="border-radius: 60px;">
                            </div>
                            <div class="col informative-header text-center" style="font-size: 12pt;">
                                <b>{{$item['qty']}} X {{$item['item']['title']}}</b>
                            </div>
                            <div class="col informative text-right">
                                <i>${{number_format($item['price'], 2)}}</i>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="card-footer" style="background: transparent !important;">
                        <div class="text-right">
                            <b><i>Ordered on {{$order->created_at}}</i></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="container">
    @if(Auth::check() && Auth::user()->seller == true)
            <div class="informative text-center container" style="margin-top:50px; margin-bottom:25px;">
                <h3 class="emphasis">Order Management</h3>
                @if($order->status == 'pending' || $order->status == 'shipped')
                <form action="{{ route('admin.editOrder', $order->id) }}" method="post">
                    <!-- Status -->
                    <div class="text-center">
                        <div>
                            <div class="form-group">
                                <label for="normal-input" class="form-control-label">Set or edit Order status</label>
                                <div>
                                    <select name="status" id="status">
                                        <option value="pending" {{$order->status=='pending' ? 'selected' : ''}}>
                                            Pending
                                        </option>
                                        <option value="shipped" {{$order->status=='shipped' ? 'selected' : ''}}>
                                            Shipped
                                        </option>
                                        <option value="cancelled" {{$order->status=='cancelled' ? 'selected' : ''}}>
                                            Cancelled
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Tracking Number -->
                    <center>
                    <div class="text-center">
                        <div>
                            <div class="form-group">
                                <label for="normal-input" class="form-control-label">Add or Update tracking number</label>
                                <input name="tracking_number" type="text" id="" class="form-control"
                                    value="{{$order->tracking_number}}" />
                            </div>
                        </div>
                    </div>
                    </center>
                    <button class="btn btn-success" type="submit">
                        Update Order
                    </button>
                    @csrf
                </form>
                @else
                <span>Order has been {{$order->status}}, no further action required.</span>
                @endif
            </div>
        @endif
        @else
        <div class="text-center">
        <h1>We're sorry, <br> we could not find your order.</h1>
        </div>
        @endif
</div>
@endsection