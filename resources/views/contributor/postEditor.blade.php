@extends('layouts.admin')

@section('title')
Matrilineal Nerd - Contributor
@endsection

@section('content')
@php
use Carbon\Carbon;
use App\Configs\Constants;
$title = $post->title == null? "Enter Title Here" : $post->title;
$subtitle = $post->subtitle == null? "Enter Description Here" : $post->subtitle;
$content = $post->content == null? "Edit this text" : $post->content;

//Times
$publish_on = $post->localPublishTime();
if($publish_on != null) {
$publish_on = Carbon::parse($publish_on)->format('Y-m-d\TH:i');
}

$unpublish_on = $post->localUnpublishTime();
if($unpublish_on != null) {
$unpublish_on = Carbon::parse($unpublish_on)->format('Y-m-d\TH:i');
}

//management
$states = Constants::postStates();

//image
$image_path = $post->header_image == null?
asset('assets/img/no_image.png') : asset($post->header_image);

$ready = Auth::user()->editor == true && strcmp($post->status, $states[0]) !== 0;
$date_required = $ready? 'required' : '';
@endphp


<script src="//cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
<!-- <script src="https://cdn.ckeditor.com/ckeditor5/15.0.0/classic/ckeditor.js"></script> -->

<div class="container-fluid">
    <style>
        .masthead {
            background-image: url({{$image_path}});
            background-size: auto;
            background-repeat: no-repeat;
            background-position: center;
            min-height: 300px;
        }
    </style>
    <form style="margin-top:100px;" nonvalidate action="{{route('cont.UpdatePost', $post->id)}}" method="POST" enctype="multipart/form-data">
        <!-- Page Header -->
        <div class="masthead container-fluid">
            <div class="overlay"></div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading text-center">
                    <div>
                        <h1><strong contenteditable="true" id="title">{{$title}}</strong></h1>
                    </div>
                    <br>
                    <div>
                        <span contenteditable="true" class="subheading" id="subtitle">{{$subtitle}}</span>
                    </div>
                    <br>
                    <label for="image" class="text-left">
                        <b>Header Image</b>
                    </label>
                    <input type="file" name="image" id="normal-input" class="form-control" value="Input value" placeholder="Add a Header Image" />
                </div>
            </div>
        </div>
        <br>
        <div class="text-center">
            @php
            if($post->user == null) {
            $name = Auth::user()->name;
            }else {
            $name = $post->user->name;
            }
            @endphp
            <i>Written By <a href="#">{{$name}}</a></i> &ensp; &#9825; &ensp;
            <span>{{$post->created_at}}</span>
        </div>
        <br>
        <div class="container">
            <input type="hidden" name="title" id="title-input">
            <input type="hidden" name="subtitle" id="subtitle-input">
            <div>
                <label for="editor1">
                    <span class="label label-primary">
                        <h4>Write your content here...</h4>
                    </span>
                </label>
                <textarea name="editor1" id="content" rows="20">{{$content}}</textarea>
                <script>
                    CKEDITOR.replace('editor1');
                    CKEDITOR.config.allowedContent = true;
                </script>
            </div>
            <br>
            <div class="text-center">
                <div style="margin-bottom:15px;">
                    <small>Times are represented as Pacific Standard Time</small>
                    <br>
                    <small><b>This site publishes articles from 8am - 12pm</b></small>
                </div>
                <label for="publish-on">Publish On:</label>
                <input type="datetime-local" id="publish-on" name="publish_on" value="{{$publish_on}}" {{$date_required}}>

                <label for="unpublish-on">Unpublish On:</label>
                <input type="datetime-local" id="unpublish-on" name="unpublish_on" value="{{$unpublish_on}}">
            </div>
            <hr>
            <br>
            <div class="text-center">
                @if(strcmp($post->status, $states[0]) == 0 || strcmp($post->status, $states[3] == 0))
                <button type="submit" class="btn btn-primary" name="action" value="save">Save Post
                </button>
                @endif
                <a href="{{route('cont.PreviewPost', $post->id)}}" class="btn btn-warning">Preview Post</a>
                @if(strcmp($post->status, $states[3]) !== 0 &&
                strcmp($post->status, $states[2]) !== 0)
                <button type="submit" class="btn btn-success" name="action" value=submit>Submit Post</button>
                @else
                <br>
                <div style="margin-top:20px;">
                    <h3 style="color:green;">Article Submitted</h3>
                </div>
                @endif
            </div>

            @if(strcmp($post->status, $states[0]) != 0 && Auth::user()->editor == true)
            <br>
            <div class="text-center container">
                <h3 class="emphasis text-center">
                    Article Management
                </h3>
                <!-- Status -->
                <div class="text-center">
                    <div>
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Update Article Status</label>
                            <div>
                                <select name="post_status" id="status">
                                    @foreach($states as $state)
                                    <option value="{{$state}}" {{$post->status== $state ? 'selected' : ''}}>
                                        {{$state}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-success" name="action" value=editor-save>Update Article</button>
            </div>
            @endif
            <br>
        </div>
        @csrf


    </form>
</div>
@endsection

@section('scripts')
<script>
    $('#title').on('DOMSubtreeModified', function() {
        $('#title-input').val($('#title').text());
    })

    $('#subtitle').on('DOMSubtreeModified', function() {
        $('#subtitle-input').val($('#subtitle').text());
    })
</script>
@endsection