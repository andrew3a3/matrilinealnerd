@extends('layouts.admin')
@section('title')
Matrilineal Nerd - Products
@endsection


@section('content')
<div class="content">
    <div class="card">
        <div class="card-header bg-light">
            <b>Products</b>

            <a href="{{ route('adminNewProduct')}}" class="btn btn-primary btn-sm">New Product</a>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Thumbnail</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td class="text-nowrap"><a
                                    href="{{route('shop.singleProduct', $product->slug) }}">{{$product->title}}</td>
                            <td><img src="{{asset($product->image)}}" width=100 alt=""></td>
                            <td>${{$product->price}}</td>
                            <td>{{$product->description}}</td>
                            <!---<td>{{ \Carbon\Carbon::parse($product->created_at)->diffForHumans() }}</td>
                                        <td>{{ \Carbon\Carbon::parse($product->updated_at)->diffForHumans() }}</td>-->
                            <td>
                                <a href="{{ route('adminEditProduct', $product->id) }}" class="btn-warning">
                                    <i class="icon icon-pencil"></i></a>
                                <form method="POST" id="deleteProduct-{{ $product->id}}"
                                    action="{{route('adminDeleteProduct', $product->id)}}">@csrf
                                    <button type="button" class="btn btn-danger" data-toggle="modal"
                                        data-target="#deleteProductModal-{{$product->id}}">
                                        x
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

<!--Modal -->
@foreach($products as $product)
<div class="modal fade" id="deleteProductModal-{{$product->id}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger border-0">
                <h5 class="modal-title text-white">You are about to delete {{$product->title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body p-5">
                Are you sure?
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No, Keep it</button>
                <form method="POST" id="deleteProduct-{{$product->id}}"
                    action="{{route('adminDeleteProduct', $product->id)}}">
                    @csrf
                    <button type="submit" class="btn btn-primary">Yes, Delete it</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection