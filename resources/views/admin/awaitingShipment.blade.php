@extends('layouts.admin')

@section('title')
Matrilineal Nerd - Awaiting Shipment
@endsection

@section('content')
<div class="container text-center" style="margin-top:70px;">
        <h1 class="emphasis">Awaiting Shipment</h1>
        <hr />
        @if(count($orders) > 0)
            <h3 class="informative-header">Here is a list of orders currently awaiting shipment</h3>
        @else
            <h3 class="informative-header">There are currently no orders awaiting shipment</h3>
        @endif
        <br>
            @foreach($orders as $order)
            <div class="informative" style="margin-bottom: 35px;">
                <a href="{{route('order-summary', $order->id)}}" style="color:black !important;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col emphasis">
                                    <h5>Order# {{$order->id}}</h5>
                                </div>

                                <div class="col informative text-center">
                                    @php
                                    $total = $order->post_price + $order->tax + $order->shipping;
                                    @endphp
                                    <span>Total Price ${{number_format($total, 2)}}</span>
                                </div>
                                <div class="col informative text-right">
                                    <small>Ordered on {{$order->created_at}}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
    </div>
@endsection