@extends('layouts.admin')

@section('title')
Matriliean Nerd - New Product
@endsection


@section('content')
<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                                New Product
                            </div>
                            @if(Session::has('success'))
                                <div class="alert alert-success">{{Session::get('success')}}</div>
                            @endif

                            @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form
                                action="{{ route('adminNewProduct') }}"
                                method="POST"
                                enctype="multipart/form-data"
                            >
                            @csrf
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label
                                                    for="normal-input"
                                                    class="form-control-label"
                                                    >Thumbnail</label
                                                >
                                                <input
                                                    type="file"
                                                    name="thumbnail"
                                                    id="normal-input"
                                                    class="form-control"
                                                    value="Input value"
                                                    placeholder="Add a thumbnail (optional)"
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label
                                                    for="normal-input"
                                                    class="form-control-label"
                                                    >Product Name</label
                                                >
                                                <input
                                                    name="title"
                                                    id="normal-input"
                                                    class="form-control"
                                                    placeholder="Add product name"
                                                />
                                            </div>
                                        </div>
                                    </div>

                                     <!-- CATEGORY -->
                                     <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label
                                                    for="normal-input"
                                                    class="form-control-label"
                                                    >Product Category</label
                                                >
                                                <div>
                                                <?php
                                                    use App\Configs\Constants;
                                                ?>

                                                @php
                                                    $categories = Constants::categories(); 
                                                    $count = 0;
                                                @endphp
                                                <select name="category" id="category">
                                                    @foreach($categories as $category)
                                                
                                                        <option value="{{$category}}" {{($count == 0) ? 'selected' : ''}}>
                                                            {{$category}}
                                                        </option>

                                                        @php
                                                            $count++;
                                                        @endphp
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                     <!-- Product Weight-->
                                     <div class="row" style="margin-top:20px;">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label
                                                    for="normal-input"
                                                    class="form-control-label"
                                                    >Product Weight</label
                                                >
                                                <div class="row">
                                                    <div class="col">
                                                        <label
                                                        for="normal-input"
                                                        class="form-control-label"
                                                        >
                                                            <small>Pound(s)</small>
                                                        </label
                                                        >
                                                        <input
                                                        name="pounds"
                                                        type="number"
                                                        step="1"
                                                        min="0"
                                                        id="normal-input"
                                                        class="form-control"
                                                        value="0"/>
                                                    </div>

                                                    <div class="col">
                                                        <label
                                                        for="normal-input"
                                                        class="form-control-label"
                                                        >
                                                            <small>Ounce(s)</small>
                                                        </label
                                                        >
                                                        <input
                                                        name="ounces"
                                                        type="number"
                                                        step=".01"
                                                        min="0"
                                                        id="normal-input"
                                                        class="form-control"
                                                        value="1"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label
                                                    for="placeholder-input"
                                                    class="form-control-label"
                                                    >Description</label
                                                >

                                                <textarea name="editor1" id="content" rows="5">Add a description</textarea>
                                                <script>
                                                CKEDITOR.replace('editor1');
                                                </script>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label
                                                    for="normal-input"
                                                    class="form-control-label"
                                                    >Price</label
                                                >
                                                <input
                                                    name="price"
                                                    type="number"
                                                    step=".01"
                                                    id=""
                                                    min="0"
                                                    class="form-control"
                                                    value="10.00"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <button
                                        class="btn btn-success"
                                        type="submit"
                                    >
                                        Create Product
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection