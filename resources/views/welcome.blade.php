@extends('layouts.master')

@section('title')
Matrilineal Nerd
@endsection

@section('content')
<div class="container">
    <style>
    body {
        width: initial !important;
        height: initial !important;
        background-color:#F1C60E;
        background-image: url("{{asset('site-images/home.gif')}}");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 75%;
    }

    </style>
    <div id="message">
        <a href="{{route('register')}}">
            <button type="button" class="btn btn-outline-dark btn-lg emphasis">Join Now</button>
        </a>
    </div>
</div>
@endsection

@section('scripts')
@endsection