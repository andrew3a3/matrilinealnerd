@extends('layouts.master')

@section('title')
Matrilineal Nerd - Blog
@endsection

@section('content')
<!-- Page Header -->
<link href="https://fonts.googleapis.com/css?family=Manjari&display=swap" rel="stylesheet">
<center>
    <header class="masthead" style="background-image: url(https://i.imgur.com/Z1K4iRD.png)">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading text-center">
                        <!--<strong>Content for the soul!</strong>-->
                    </div>
                </div>
            </div>
        </div>
    </header>
</center>
<br>
<div class="container-fluid">
    @if(count($blogs) > 0)
    <div class="wrapper">
        @foreach($blogs as $blog)
        <div class="blog-item">
            <div class="blog-preview-content informative text-center">
                <a href="{{ route('blog.singlePost', $blog->slug) }}">
                    <img src="{{$blog->header_image}}" width="250" alt="">
                    <div id="blog-preview-title" class="text-center emphasis maroon">
                        <h2>{{$blog->title}}</h2>
                    </div>

                    <div class="text-center informative-light">
                        <small style="color: black !important;">By {{$blog->user->name}} &#183;
                            {{$blog->styledLocalPublishTime()}}</small>
                    </div>
                    <p>
                        {{$blog->subtitle}}
                    </p>
                </a>
            </div>
        </div>
        @endforeach
    </div>

    <div class="text-center">
        {{ $blogs->links() }}
    </div>
    @else
    <center>
        <h1 class="emphasis">
            No blog content yet, <br>
            but don't worry it's coming very soon!
        </h1>
    </center>
    @endif
</div>
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/css/dynamic-grid.css')}}">
@endsection