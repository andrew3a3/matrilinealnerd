@extends('layouts.master')

@section('title')
Matrilineal Nerd - {{$post->title}}
@endsection

@section('content')
<div class="container fluid">
    <div id="blog-head">
        <div id="blog-header">
            <div>
                <div>
                    <button class="maroon emphasis" onclick="goBack()">
                        <i class="fas fa-chevron-left"></i>
                        <b>Back</b>
                    </button>
                </div>
            </div>
            <div class="text-center">
                <div class="maroon emphasis">
                    <h1>{{$post->title}}</h1>
                </div>
                <div>
                    <p class="informative-header">
                        <small>
                            <i>{{$post->subtitle}}</i>
                        </small>
                    </p>
                </div>
            </div>
            <!-- <div id="desktop-share" class="text-right">
                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0&appId=1504323283031994&autoLogAppEvents=1">
                </script>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

                <div style="margin-bottom: 10px; margin-top: 25px;" class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a>
                </div>
                <div>
                    <a class="twitter-share-button" href="https://twitter.com/intent/tweet" data-size="large">
                        Tweet</a>
                </div>
            </div> -->
        </div>
        <center>
            <div class="masthead" style="background-image: url('{{ asset($post->header_image) }}')">
                <div class="overlay"></div>
                <!-- <span id="masthead-category">
                <span>{{$post->category}}</span>
            </span>

            <div id="blog-like" class="text-right">
                <a href="">
                    <i class="far fa-heart fa-2x"></i>
                </a>
            </div> -->
            </div>
        </center>
        <br>
        <div class="text-center informative-light" id="author-info">
            @php
            use Carbon\Carbon;
            if($post->user == null) {
            $name = 'Jane Doe';
            }else {
            $name = $post->user->name;
            }
            @endphp
            <small>Written by {{$name}} &bull; {{$post->created_at}}</small>
            <!-- <div id="mobile-share" class="text-center row">
                <div class="col">
                    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0&appId=1504323283031994&autoLogAppEvents=1">
                    </script>
                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

                    <div style="margin-bottom: 10px; margin-top: 25px;" class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a>
                    </div>
                </div>
                <div class="col">
                    <div>
                        <a class="twitter-share-button" href="https://twitter.com/intent/tweet" data-size="small">
                            Tweet</a>
                    </div>
                </div>
            </div> -->
            <hr>
        </div>
    </div>
    <div class="container informative" id="content">
    </div>

    <!-- comments container -->
    <!-- <div class="comment-section container text-center">
        <hr>
        <h2 style="color:#999">Join the Conversation</h2>
        <div class="comment-content">
            @php
            $comments = [];
            @endphp
            @if(count($comments) > 0)
            @foreach($comments as $comment)
            <center>
                <div class="comment">
                    <h5 class="text-left">{{$comment->creator}}</h5>
                    <div class="single-comment">
                        <p>
                            {{$comment->content}}
                            <div class="text-right">
                                <span style="margin-right: 25px;">
                                    <i class="far fa-thumbs-up maroon"></i>
                                    @if($comment->likes > 0)
                                    <span class="badge badge-pill badge-dark maroon">{{$comment->likes}}</span>
                                    @endif
                                </span>
                                <span>
                                    <i class="far fa-thumbs-down maroon"></i>
                                    @if($comment->dislikes > 0)
                                    <span class="badge badge-pill badge-dark maroon">{{$comment->dislikes}}</span>
                                    @endif
                                </span>
                            </div>
                        </p>
                    </div>
                    @php
                    $commentCreateDate = "12.2.2020";
                    @endphp
                    <div class="text-right comment-create-date"><i>{{$commentCreateDate}}</i>
                    </div>
                </div>
            </center>
            @endforeach
            @endif
        </div>
        <center>
            <form class="comment-input">
                <div class="form-group">
                    <textarea class="form-control" id="comment-input" name="commment" placeholder="Write a comment" rows="3"></textarea>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-outline-dark">
                        <i class="far fa-comment"></i>
                        Submit
                    </button>
                </div>
            </form>
        </center>
    </div> -->
</div>
@endsection

@section('scripts')

@php
$content = json_encode($post->content);
@endphp
<script>
    $('#content').html(<?php echo $content ?>);

    function goBack() {
        window.history.back();
    }
</script>

@endsection