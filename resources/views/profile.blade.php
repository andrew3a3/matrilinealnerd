@extends('layouts.master')

@section('title')
Matrilineal Nerd - My Profile
@endsection

@section('content')
    <div class="container" style="margin-top:40px;">
        <h1>Profile</h1>
        <hr />
        <h2>My Orders</h2>
            @foreach($orders as $order)
            @php
                        $color = "#999999";
                        switch($order->status) {
                            case 'shipped':
                            $color = '#FFC576';
                            break;

                            case 'delivered':
                            $color = '#57BC91';
                            break;

                            case 'cancelled':
                            $color = '#DC143C';
                            break;
                        }
            @endphp
            <div style="margin-bottom: 25px;">
                <a href="{{route('order-summary', $order->id)}}" style="color:black !important;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col emphasis">
                                    <h5>Order# {{$order->id}}</h5>
                                </div>
                                <div class="col informative text-center">
                                    @php
                                    $total = $order->post_price + $order->tax + $order->shipping;
                                    @endphp
                                    <span>Total Price ${{number_format($total, 2)}}</span>
                                </div>
                                <div class="col informative-header text-center" style="color:{{$color}};">
                                    {{$order->status}}
                                </div>
                                <div class="col informative text-right">
                                    <small>Ordered on {{$order->created_at}}</small>
                                </div>
            </div>
                            <!--<ul class="list-group">
                                    @foreach($order->cart->items as $item)
                                    <span class="badge">${{$item['price']}}</span>
                                        {{$item['item']['title']}} | {{$item['qty']}} Units
                                    @endforeach
                                </ul>-->
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
    </div>
@endsection