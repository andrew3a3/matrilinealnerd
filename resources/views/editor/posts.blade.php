@extends('layouts.admin')

@section('title')
Matrilineal Nerd - Editor ~ Pending Approval
@endsection

@section('content')
<div class="container">
    <style>
    .container {
        margin-top: 75px;
    }
    </style>
    <center>
        <h1><strong>Articles Awaiting Approval</strong></h1>
        <br>
    </center>
    <br>
    <div class="row">
        @foreach ($posts as $post)
        @php
        $title = $post->title == null? "No Title Yet." : $post->title;
        $subtitle = $post->subtitle == null? "No sub description yet" : $post->subtitle;
        $image_path = $post->header_image == null? asset('assets/img/no_image.png') : asset($post->header_image);
        @endphp
            <div class="col-4">
                <a href="{{ route('cont.SinglePost', $post->id) }}" title="View Article">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{$image_path}}" alt="Image">
                        <div class="card-body">
                            <h5 class="card-title">{{$title}}</h5>
                            <p class="card-text"><i>{{$subtitle}}</i></p>
                        </div>
                </a>
                <div class="card-footer">
                        <div class="row">
                            @php
                                $badgeType = "badge-primary";

                                switch($post->status) {
                                case "ready for review":
                                $badgeType = "btn-info";
                                break;

                                case "authorized":
                                $badgeType = "btn-success";
                                break;

                                case "needs revision":
                                $badgeType = "btn-warning";
                                break;

                                case "cancelled":
                                $badgeType = "btn-secondary";
                                break;

                                case "retracted":
                                $badgeType = "btn-danger";
                                break;
                                }

                                $publishDate = $post->publish_on == null? "No Publish Date Yet." : $post->publish_on;
                            @endphp
                            <div class="col">
                                @if($post->published)
                                    <span class="badge btn-success">Published</span>
                                @else
                                    <span class="badge {{$badgeType}}">{{$post->status}}</span>
                                @endif
                            </div>
                            <div class="col"><small>{{$publishDate}}</small></div>
                        </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

@foreach($posts as $post)
<div class="modal fade" id="deletePostModal-{{$post->id}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger border-0">
                <h5 class="modal-title text-white">You are about to delete the article {{$post->title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body p-5">
                Are you sure?
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No, Keep it</button>
                <form method="POST" id="deletePost-{{$post->id}}"
                    action="{{route('cont.deletePost', $post->id)}}">
                    @csrf
                    <button type="submit" class="btn btn-primary">Yes, Delete it</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection