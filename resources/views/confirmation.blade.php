@extends('layouts.master')

@section('title')
Matrilineal Nerd - Purchase Confirmation
@endsection

@section('content')
    <style>
        body {
            background-image: linear-gradient(white, rgba(190, 244, 219, .1));
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center;
            background-size: cover;
        }

        @media (max-width: 768px) {
            #thank-you {
                font-size: 70px !important;
            }
        }

        #thank-you {
            margin-top: 50px;
            font-size: 8em;
            text-shadow: 2px 2px 2px #999999;
        }

        #actions {
            margin-left: 2em;
            margin-top: 25px;
        }

        #actions>div:nth-child(1) {
            font-size: 14pt;
        }

        #actions>div:nth-child(2) {
            font-size: 20pt;
        }

        #actions div {
            margin-bottom: 20px;
        }

    </style>
    <div class="text-center emphasis" id="thank-you">
        Thank you for <br> shopping with us
    </div>

    <div id="actions" class="text-center">
        <div class="informative-header">
            Look out for an email about your purchase from us very soon!
        </div>
        <div>
            <a href="{{route('index')}}">
                Continue on
            </a>
        </div>
    </div>
@endsection